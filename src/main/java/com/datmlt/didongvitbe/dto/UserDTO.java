package com.datmlt.didongvitbe.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class UserDTO {
    @Data
    public static class Update {
        private Long id;
        private String fullName;
        private String phoneNumber;
        private String dateOfBirth;
    }
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Response {
        private Long id;
        private String fullName;
        private String phoneNumber;
        private String dateOfBirth;
        private String avatar;
        private String createAt;
        private Long accountId;
    }
}
