package com.datmlt.didongvitbe.dto.forDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class SpecificationDetailDTO {
    private Long id;
    private String name;
    private String nameDetail;
    private String description;
}
