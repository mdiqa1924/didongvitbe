package com.datmlt.didongvitbe.dto.forDetail;

import com.datmlt.didongvitbe.entity.Product;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class DProductDTO {
    //private Long id;
    private Product product;
    private List<Map<String, Object>> specificationDetails;
    private Set<String> imageLinks;
}
