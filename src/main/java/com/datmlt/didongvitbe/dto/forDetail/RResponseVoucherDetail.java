package com.datmlt.didongvitbe.dto.forDetail;

public class RResponseVoucherDetail {
    private double total;
    private String msg;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

