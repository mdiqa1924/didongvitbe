package com.datmlt.didongvitbe.dto;

import java.util.List;

public class UProductColorDTO {
    private Long productId;
    private List<Long> colors;

    public Long getProductId() {
        return productId;
    }

    public List<Long> getColors() {
        return colors;
    }
}
