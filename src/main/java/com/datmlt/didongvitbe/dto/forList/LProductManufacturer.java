package com.datmlt.didongvitbe.dto.forList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class LProductManufacturer {

    private Long id;

    private String name;

    private Long price;

    private Double rate;

    private String imageLinks;
}
