package com.datmlt.didongvitbe.dto.forList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LRateDTO {
    private String fullName;
    private String phoneNumber;
    private String evaluate;
    private Double star;
    private String Time;
    private Long productId;
    private Set<String> imageRate;
    private Double averageRating;
}
