package com.datmlt.didongvitbe.dto.forList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class LProductDTO {

    private Long id;

    private String name;

    private Long price;

    private Double rate;

    private String Conditions;

    private String dateWarranty;

    private String imageLinks;
}
