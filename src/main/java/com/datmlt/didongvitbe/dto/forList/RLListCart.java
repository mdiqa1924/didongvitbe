package com.datmlt.didongvitbe.dto.forList;

public class RLListCart {
    private Long productId;
    private Double total;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double percent) {
        this.total = percent;
    }
}

