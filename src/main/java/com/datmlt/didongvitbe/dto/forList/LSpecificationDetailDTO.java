package com.datmlt.didongvitbe.dto.forList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class LSpecificationDetailDTO {
    private Long id;
    private String nameDetail;
    private String description;
}
// response a list detail sepcification group by name specification