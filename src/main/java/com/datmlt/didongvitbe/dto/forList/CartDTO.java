package com.datmlt.didongvitbe.dto.forList;

import com.datmlt.didongvitbe.dto.forDetail.CartDetailDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CartDTO {
    private Long userId;
    private List<CartDetailDTO> cartDetailDTOList;
}
