package com.datmlt.didongvitbe.dto;

import com.datmlt.didongvitbe.entity.Orders;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CheckDiscount {
    private Orders orderId;
}
