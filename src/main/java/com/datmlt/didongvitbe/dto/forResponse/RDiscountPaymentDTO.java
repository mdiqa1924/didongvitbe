package com.datmlt.didongvitbe.dto.forResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class RDiscountPaymentDTO {
    private Long id;
    private String discountPaymentName;
    private Double percent;
    private String status;
    private Long paymentId;

}
