package com.datmlt.didongvitbe.dto.forResponse;

import com.datmlt.didongvitbe.entity.DiscountPayment;
import com.datmlt.didongvitbe.entity.Orders;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class RDiscountPaymentDetailDTO {
    private Long id;
    private Orders orderId;
    private DiscountPayment discountPaymentId;
}
