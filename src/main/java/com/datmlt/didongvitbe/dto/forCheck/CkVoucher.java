package com.datmlt.didongvitbe.dto.forCheck;

public class CkVoucher {
    private Long total;

    private String voucherCode;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
}
