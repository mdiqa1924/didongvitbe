package com.datmlt.didongvitbe.dto;

import java.util.ArrayList;
import java.util.List;

public class CProductColorDTO {
    private Long productId;
    private List<Long> colors = new ArrayList<>();

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<Long> getColors() {
        return colors;
    }

    public void setColors(List<Long> colors) {
        this.colors = colors;
    }
}
