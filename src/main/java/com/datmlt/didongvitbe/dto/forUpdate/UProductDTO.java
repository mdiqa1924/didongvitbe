package com.datmlt.didongvitbe.dto.forUpdate;

import com.datmlt.didongvitbe.entity.CategoryC;
import com.datmlt.didongvitbe.entity.Manufacturer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class UProductDTO {
    private Long id;

    private String name;

    private String description;

    private Long price;

    private Integer stock;

    private String conditions;

    private Long discount;

    private String dateWarranty;

    private Double rate;
    private CategoryC categoryC;
    private Manufacturer manufacturer;
    private MultipartFile[] files;

    private Set<String> imageLinks;
}
