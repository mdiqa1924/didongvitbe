package com.datmlt.didongvitbe.dto.forUpdate;

import lombok.Data;

@Data
public class USpecificationDetailDTO {
    private long id;
    private String nameDetail;
    private String description;

}
