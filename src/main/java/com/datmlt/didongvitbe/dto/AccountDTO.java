package com.datmlt.didongvitbe.dto;

import com.datmlt.didongvitbe.utils.constant.AuthProvider;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class AccountDTO {


    @Data
    @AllArgsConstructor
    public static class EmailResult {
        private boolean isSuccess;
        private String message;
    }
    @Data
    public static class ChangePassword {
        private String email;
        private String currentPassword;
        private String newPassword;
        private String verificationCode;
    }
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Response {
        private Long id;
        private String email;
        private String status;
        private String role;
        private AuthProvider provider;
        private String providerId;
        private UserDTO.Response user;
    }
}
