package com.datmlt.didongvitbe.dto.forCreate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDTO {
    private String phoneNumber;
    private String fullName;
    private String content;
    private String time;
    private Long productId;

}
