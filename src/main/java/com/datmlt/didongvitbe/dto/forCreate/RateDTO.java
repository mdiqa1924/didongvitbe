package com.datmlt.didongvitbe.dto.forCreate;

import com.datmlt.didongvitbe.entity.Rate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RateDTO {
    private Rate rate;
    private Set<String> imageRate;
    private Double averageRating;
}
