package com.datmlt.didongvitbe.dto.forCreate;

import com.datmlt.didongvitbe.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ProductDTO {
    private Product product;
    private MultipartFile[] files;
    private List<Long> colors;

}
