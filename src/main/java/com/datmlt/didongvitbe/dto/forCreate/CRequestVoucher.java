package com.datmlt.didongvitbe.dto.forCreate;

public class CRequestVoucher {
    private String voucherName;
    private String voucherCode;
    private Double discountValue;

    private Double condition;

    private Integer quantity;

    private Integer stock;

    private String dateStart;

    private String dateEnd;

    public String getVoucherName() {
        return voucherName;
    }

    public Double getDiscountValue() {
        return discountValue;
    }

    public Double getCondition() {
        return condition;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Integer getStock() {
        return stock;
    }

    public String getDateStart() {
        return dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    @Override
    public String toString() {
        return "CRequestVoucher{" +
                "voucherName='" + voucherName + '\'' +
                ", voucherCode='" + voucherCode + '\'' +
                ", discountValue=" + discountValue +
                ", condition=" + condition +
                ", quantity=" + quantity +
                ", stock=" + stock +
                ", dateStart='" + dateStart + '\'' +
                ", dateEnd='" + dateEnd + '\'' +
                '}';
    }
}
