package com.datmlt.didongvitbe.dto.forCreate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class DiscountPaymentDetailDTO {
    private Long id;
    private Long orderId;
    private Long discountPaymentId;
}
