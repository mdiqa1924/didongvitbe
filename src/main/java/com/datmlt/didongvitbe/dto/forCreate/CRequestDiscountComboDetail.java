package com.datmlt.didongvitbe.dto.forCreate;

public class CRequestDiscountComboDetail {
    private Long categoryId1;
    private Long categoryId2;
    private Long discountComboId;

    public Long getCategoryId1() {
        return categoryId1;
    }

    public void setCategoryId1(Long categoryId1) {
        this.categoryId1 = categoryId1;
    }

    public Long getCategoryId2() {
        return categoryId2;
    }

    public void setCategoryId2(Long categoryId2) {
        this.categoryId2 = categoryId2;
    }

    public Long getDiscountComboId() {
        return discountComboId;
    }

    public void setDiscountComboId(Long discountComboId) {
        this.discountComboId = discountComboId;
    }
}
