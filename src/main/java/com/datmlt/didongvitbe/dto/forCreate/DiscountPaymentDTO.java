package com.datmlt.didongvitbe.dto.forCreate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class DiscountPaymentDTO {
    private Long id;
    private String discountPaymentName;
    private Double percent;
    private String status;
    private Long paymentId;
}
