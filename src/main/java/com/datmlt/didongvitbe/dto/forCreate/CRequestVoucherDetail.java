package com.datmlt.didongvitbe.dto.forCreate;

public class CRequestVoucherDetail {
    private Long ordersID;

    private String voucherCode;

    public Long getOrdersID() {
        return ordersID;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

}
