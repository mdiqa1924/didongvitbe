package com.datmlt.didongvitbe.dto.forCreate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CCartDetailDTO {
    private Long id;
    private Integer quantity;
    private Long price;
    private Long unitPrice;
    private Long CartId;
    private Long productId;
    private Long productColor;
}
