package com.datmlt.didongvitbe.dto.forCreate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CSpecificationDetailDTO {
    private Long id;
    private String name;
    private String nameDetail;
    private String description;
    private Long productId;
}
