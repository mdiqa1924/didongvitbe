package com.datmlt.didongvitbe.dto.forCreate;

import com.datmlt.didongvitbe.entity.Rate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class RateResponseDTO {
    private Rate rate;
    private Set<String> link;
    private Double averageRating;
}
