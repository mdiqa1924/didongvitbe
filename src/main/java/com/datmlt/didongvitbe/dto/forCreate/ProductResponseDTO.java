package com.datmlt.didongvitbe.dto.forCreate;

import com.datmlt.didongvitbe.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ProductResponseDTO {
    private  Product product;
    private Set<String> imageLinks;
    private List<Map<String, Object>> productColors;
}
