package com.datmlt.didongvitbe.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class RegisterModel {
    private String fullName;
    private String email;
    private String password;
    private String phoneNumber;
}
