package com.datmlt.didongvitbe.config;

import com.datmlt.didongvitbe.security.jwt.JwtAuthEntryPoint;
import com.datmlt.didongvitbe.security.jwt.JwtAuthFilter;
import com.datmlt.didongvitbe.security.oauth2.service.CustomOAuth2AccountService;
import com.datmlt.didongvitbe.security.oauth2.service.HttpCookieOAuth2AuthorizationRequestRepository;
import com.datmlt.didongvitbe.security.oauth2.service.OAuth2AuthenticationFailureHandler;
import com.datmlt.didongvitbe.security.oauth2.service.OAuth2AuthenticationSuccessHandler;
import com.datmlt.didongvitbe.service.CustomAccountDetailsService;
import com.datmlt.didongvitbe.service.CustomAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)  extends GlobalMethodSecurityConfiguration
public class WebConfigSecurity {

    @Autowired
    private CustomAccountDetailsService customAccountDetailsService;
    @Autowired
    private CustomOAuth2AccountService customOAuth2AccountService;
    @Autowired
    private OAuth2AuthenticationSuccessHandler oauth2AuthenticationSuccessHandler;
    @Autowired
    private OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;

    @Autowired
    private DataSource dataSource;

    @Bean
    public JwtAuthEntryPoint http401ErrorEntryPoint() {return new JwtAuthEntryPoint();}

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Bean
    public JwtAuthFilter tokenAuthenticationFilter() {
        return new JwtAuthFilter();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return super.encode(rawPassword);
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return super.matches(rawPassword, encodedPassword);
            }
        };
    }


    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
        tokenRepository.setDataSource(dataSource);
        return tokenRepository;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return new ProviderManager(Arrays.asList(authenticationProvider()));
    }

    @Bean
    public CustomAuthenticationProvider authenticationProvider() {
        CustomAuthenticationProvider authenticationProvider = new CustomAuthenticationProvider();
        authenticationProvider.setCustomAccountDetailsService(customAccountDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    private RequestMatcher publicMatchers(){
        List<RequestMatcher> matchers = Arrays.asList(
                new AntPathRequestMatcher("/"),
                new AntPathRequestMatcher("/v3/api-docs"),
                new AntPathRequestMatcher("/app/**/*.{js,html}"),
                new AntPathRequestMatcher("/content/**"),
                new AntPathRequestMatcher("/error"),
                new AntPathRequestMatcher("/configuration/ui"),
                new AntPathRequestMatcher("/swagger-resources/**"),
                new AntPathRequestMatcher("/configuration/security"),
                new AntPathRequestMatcher("/swagger-ui.html"),
                new AntPathRequestMatcher("/webjars/**"),
                new AntPathRequestMatcher("/error"),
                new AntPathRequestMatcher("/auth/signup"),
                new AntPathRequestMatcher("/oauth2/**"),
                new AntPathRequestMatcher("/webjars/**"),
                new AntPathRequestMatcher("/favicon.ico"),
                new AntPathRequestMatcher("/**/*.png"),
                new AntPathRequestMatcher("/**/*.gif"),
                new AntPathRequestMatcher("/**/*.svg"),
                new AntPathRequestMatcher("/**/*.jpg"),
                new AntPathRequestMatcher("/**/*.html"),
                new AntPathRequestMatcher("/**/*.css"),
                new AntPathRequestMatcher("/**/*.js")
        );
        return new OrRequestMatcher(matchers);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http.csrf(csrf -> csrf.disable())
                .exceptionHandling(exceptionHandling -> exceptionHandling.authenticationEntryPoint(http401ErrorEntryPoint()))

                .authorizeHttpRequests(authorize -> authorize
                                .requestMatchers(publicMatchers()).permitAll()
                                .requestMatchers(HttpMethod.OPTIONS,"/**").permitAll()
//                                .requestMatchers("/api/user/**").permitAll()
                                //API Account
                                .requestMatchers("/api/account/login").permitAll()
                                .requestMatchers("/api/account/register").permitAll()
                                .requestMatchers("/api/account/").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/account/changeRole/**").hasRole("ADMIN")
                                .requestMatchers("/api/account/changeStatus/**").hasRole("ADMIN")
                                .requestMatchers("/api/account/changePassword").hasAnyRole("CUSTOMER", "MANAGER")
                                .requestMatchers("/api/account/reset").permitAll()
                                .requestMatchers("/api/account/reset/confirm").permitAll()
                                .requestMatchers("/api/account/role/").permitAll()
                                .requestMatchers("/api/account/search/**").hasAnyRole("ADMIN", "MANAGER")
                                //API User
//                                .requestMatchers("/api/user").permitAll()
//                                .requestMatchers("/api/user/update").permitAll()
                                .requestMatchers("/api/user/users").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/user/account").hasAnyRole("ADMIN", "MANAGER")
                                //API Cart
                                .requestMatchers("/carts/").permitAll()
//                                .requestMatchers("/carts/users").permitAll()
//                                .requestMatchers("/carts/clear/").hasRole("ADMIN")
//                                .requestMatchers("/carts/remove-cart-detail/").hasRole("ADMIN")
//                                .requestMatchers("/carts/all-cart-details").hasRole("ADMIN")
//                                .requestMatchers("/carts/user").hasRole("ADMIN")
                                //API Category
                                .requestMatchers("/category").permitAll()
                                .requestMatchers("/category/").permitAll()
                                //API Comment
                                .requestMatchers("/api/comments/").permitAll()
                                .requestMatchers("/api/comments/createComment").permitAll()
                                .requestMatchers("/api/product/").permitAll()
                                //API DiscountCombo
                                .requestMatchers("/api/discount-combo/**").hasAnyRole("ADMIN", "MANAGER")
                                //API DiscountPayment
                                .requestMatchers("/api/discountpayment/**").hasAnyRole("ADMIN", "MANAGER")
                                //API DiscountPaymentDetail
                                .requestMatchers("/api/discountpaymentdetail/**").hasAnyRole("ADMIN", "MANAGER")
                                //API Manufacturer
                                .requestMatchers("/api/manufacturer/getAll").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/manufacturer/create").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/manufacturer/update").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/manufacturer/delete").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/manufacturer/**").permitAll()
                                //API Payment
                                .requestMatchers("/api/payment/add").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/payment/update/").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/payment/delete/").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/payment/**").permitAll()
                                //API Product Color
                                .requestMatchers("api/product_color/**").permitAll()
                                //API Product
                                .requestMatchers("/products/add").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/products/update").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/products/delete/**").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/products/admin/page").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/products/**").permitAll()
                                //API Rate
                                .requestMatchers("/api/rates").permitAll()
                                //API Specification Detail
                                .requestMatchers( HttpMethod.POST, "/details").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers( HttpMethod.PUT, "/details").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers(HttpMethod.GET,"/details").permitAll()
                                //API Store
                                .requestMatchers("/api/stores/add").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/stores/edit/").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/stores/update").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/stores/delete/").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/stores/**").permitAll()
                                //API Voucher
                                .requestMatchers("/api/voucher/add").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/voucher/get").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/voucher/getAll").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/voucher/update").hasAnyRole("ADMIN", "MANAGER")
                                .requestMatchers("/api/voucher/addVoucherDetail").hasAnyRole("ADMIN", "MANAGER")
                                //API News
//                                .requestMatchers("/api/editor/news/delete").hasRole("ADMIN")
//                                .requestMatchers("/api/editor/news/update").hasRole("ADMIN")
                                .anyRequest().authenticated()
                )

                .sessionManagement(session -> session
                                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                                .sessionFixation().changeSessionId()
                                .sessionFixation().migrateSession()
                        .maximumSessions(1)
                )


                .addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)

//                .rememberMe(remember -> remember
//                        .tokenRepository(persistentTokenRepository())
//                        .tokenValiditySeconds(604800) //1 week
//                        .rememberMeCookieName("remember-me-cookie")
//                        .rememberMeParameter("remember-me")
//                        .userDetailsService(customAccountDetailsService)
//                )

                .logout(
                        logout -> logout
                                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                                .deleteCookies("JSESSIONID")
                                .permitAll()
                )

                .oauth2Login(oauth2 -> oauth2
                        .authorizationEndpoint(endpoint -> endpoint
                                .baseUri("/oauth2/authorize")
                                .authorizationRequestRepository(cookieAuthorizationRequestRepository())
                        )
                        .redirectionEndpoint(endpoint -> endpoint
                                .baseUri("/oauth2/callback/*")
                        )
                        .userInfoEndpoint(userInfo -> userInfo
                                .userService(customOAuth2AccountService)
                        )
                        .successHandler(oauth2AuthenticationSuccessHandler)
                        .failureHandler(oAuth2AuthenticationFailureHandler)
                );

        return http.build();
    }

    private AuthorizationRequestRepository<OAuth2AuthorizationRequest> cookieAuthorizationRequestRepository() {
        return new HttpCookieOAuth2AuthorizationRequestRepository();
    }

}
