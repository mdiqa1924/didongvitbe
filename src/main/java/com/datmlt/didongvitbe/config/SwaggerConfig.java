package com.datmlt.didongvitbe.config;


import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.servers.ServerVariables;
import org.springdoc.core.configuration.SpringDocConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Collections;
import java.util.Map;


@Configuration
@Import({SpringDocConfiguration.class})
public class SwaggerConfig {
    public Info apiInfo() {
        return new Info()
                .title("DiDongVit REST API")
                .version("API Ver1.0")
                .description("API For Teams")
                .contact(new Contact()
                        .name("Di Dong Vit")
                        .url("https://api.didongvit.store")
                        .email("didongvit@gmail.com"))
                .license(new License()
                        .name("License of API")
                        .url("http://springdoc.org"))
                .extensions(Collections.singletonMap("x-external-doc", "https://didongvit.com"));
    }

    @Bean
    public OpenAPI customOpenAPI(@Value("2.1.0") String appVersion) {
        var securitySchemeName = "Authorization";
        return new OpenAPI()
                .info(apiInfo())
                .addServersItem(new Server()
                        .url("https://api.didongvit.store")
                        .description("Didongvit server")
                        .variables(new ServerVariables())
                        .extensions(Map.of("produces", "application/json", "consumes", "application/json")))
                .components(new Components().addSecuritySchemes(securitySchemeName,
                        new io.swagger.v3.oas.models.security.SecurityScheme()
                                .name(securitySchemeName)
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("Bearer")
                                .bearerFormat("JWT")
                                .in(SecurityScheme.In.HEADER)
                ))
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName));
    }
}