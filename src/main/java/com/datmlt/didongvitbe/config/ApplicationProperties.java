package com.datmlt.didongvitbe.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private final Datasource datasource = new Datasource();

    private final Security security = new Security();

    private final Google google = new Google();

    public Google getGoogle() {
        return google;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public static class Google {

        private final Client client = new Client();

        private final Resource resource = new Resource();

        public Client getClient() {
            return client;
        }

        public Resource getResource() {
            return resource;
        }

        @Data
        public static class Client {
            private String clientId;
            private String clientSecret;
            private String accessTokenUri;
            private String userAuthorizationUri;
            private List<String> scope;
        }

        @Data
        public static class Resource {
            private String userInfoUri;
        }
    }

    @Data
    public static class Datasource {

        private boolean cachePrepStmts = true;

        private int prepStmtCacheSize = 250;

        private int prepStmtCacheSqlLimit = 2048;

        private boolean useServerPrepStmts = true;

    }

    public Security getSecurity() {
        return security;
    }

    public static class Security {

        private final Jwt jwt = new Jwt();

        public Jwt getJwt() {
            return jwt;
        }

        @Data
        public static class Jwt {

            private String secret;

            private long tokenValidityInSeconds = 1800;

            private long tokenValidityInSecondsForRememberMe = 2592000;

        }
    }

    private final OAuth2 oAuth2 = new OAuth2();

    public static final class OAuth2 {
        private List<String> authorizedRedirectUris = new ArrayList<>();

        public List<String> getAuthorizedRedirectUris() {
            return authorizedRedirectUris;
        }

        public OAuth2 authorizedRedirectUris(List<String> authorizedRedirectUris) {
            this.authorizedRedirectUris = authorizedRedirectUris;
            return this;
        }
    }

    public OAuth2 getoAuth2() {
        return oAuth2;
    }
}
