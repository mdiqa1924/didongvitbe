package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.dto.forCreate.DiscountPaymentDetailDTO;
import com.datmlt.didongvitbe.entity.DiscountPaymentDetail;

import java.util.List;

public interface DiscountPaymentDetailService {
    DiscountPaymentDetail createDiscountPaymentDetail(DiscountPaymentDetailDTO discountPaymentDetailDTO);
    List<DiscountPaymentDetail> getAllDiscountPaymentDetail();
    boolean deleteAllPaymentDetail();
/*    RCheckDiscount existByOrderId(CheckDiscount id);*/
}
