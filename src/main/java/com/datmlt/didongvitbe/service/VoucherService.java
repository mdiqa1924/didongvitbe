package com.datmlt.didongvitbe.service;



import com.datmlt.didongvitbe.dto.forCreate.CRequestVoucher;
import com.datmlt.didongvitbe.dto.forList.RLResponseVoucher;
import com.datmlt.didongvitbe.entity.Voucher;

import java.util.List;

public interface VoucherService {
    Voucher insert(CRequestVoucher cRequestVoucher);
    Voucher getOne(Long id);
    List<RLResponseVoucher> getALl();
    Voucher update (Voucher voucher);
    List<RLResponseVoucher> search(String s);
    Voucher check(String code);
}
