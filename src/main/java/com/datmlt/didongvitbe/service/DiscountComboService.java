package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.dto.forCreate.CRequestDiscountCombo;
import com.datmlt.didongvitbe.entity.DiscountCombo;

import java.util.List;

public interface DiscountComboService {
    DiscountCombo insert(CRequestDiscountCombo cRequestDiscountCombo);

    DiscountCombo getOne(Long id);

    List<DiscountCombo> getAll();

    DiscountCombo update(DiscountCombo discountCombo);

    List<DiscountCombo> search(String s);

}

