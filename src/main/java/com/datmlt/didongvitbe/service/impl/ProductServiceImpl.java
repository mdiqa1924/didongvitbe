package com.datmlt.didongvitbe.service.impl;


import com.datmlt.didongvitbe.dto.forCreate.ProductDTO;
import com.datmlt.didongvitbe.dto.forCreate.ProductResponseDTO;
import com.datmlt.didongvitbe.dto.forDetail.DProductDTO;
import com.datmlt.didongvitbe.dto.forList.LProductDTO;
import com.datmlt.didongvitbe.dto.forList.LSpecificationDetailDTO;
import com.datmlt.didongvitbe.dto.forUpdate.UProductDTO;
import com.datmlt.didongvitbe.entity.*;
import com.datmlt.didongvitbe.repositories.*;
import com.datmlt.didongvitbe.service.ProductService;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private SpecificationDetailRepository specificationDetailRepository;
    @Autowired
    private ProductImageRepository productImageRepository;
    @Autowired
    private ManufacturerRepository manufacturerRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private RateImageRepository rateImageRepository;
    @Autowired
    RateRepository rateRepository;

    @Autowired
    private CategoryCRepository categoryCRepository;
    private final FirebaseImageService firebaseImageService;
    private final ProductColorRepository productColorRepository;

    public ProductServiceImpl(FirebaseImageService firebaseImageService,
                              ProductColorRepository productColorRepository) {
        this.firebaseImageService = firebaseImageService;
        this.productColorRepository = productColorRepository;
    }

    @Override
    @Transactional
    public ProductResponseDTO addProduct(ProductDTO productDto) {
        try {
            Product savedProduct = mapProductDTOToEntity(productDto);
            savedProduct = productRepository.save(savedProduct);
            saveProductImages(productDto.getFiles(), savedProduct);
            ProductResponseDTO responseDTO = new ProductResponseDTO();
            responseDTO.setProduct(savedProduct);
            responseDTO.setImageLinks(getProductImageLinks(savedProduct));

            return responseDTO;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to add product");
        }
    }

    private Product mapProductDTOToEntity(ProductDTO productDto) {
        Product product = modelMapper.map(productDto.getProduct(), Product.class);

        // Set the manufacturer
        if (productDto.getProduct().getManufacturer() != null) {
            Manufacturer manufacturer = manufacturerRepository.findById(productDto.getProduct().getManufacturer().getId()).orElse(null);
            product.setManufacturer(manufacturer);
        }

        // Set the categoryC
        if (productDto.getProduct().getCategoryC() != null) {
            CategoryC categoryC = categoryCRepository.findById(productDto.getProduct().getCategoryC().getId()).orElse(null);
            product.setCategoryC(categoryC);
        }

        return product;
    }


    private Set<String> getProductImageLinks(Product savedProduct) {
        Set<String> imageLinks = new HashSet<>();
        List<ProductImage> productImages = productImageRepository.findByProductId(savedProduct.getId());
        for (ProductImage productImage : productImages) {
            imageLinks.add(productImage.getLink());
        }
        return imageLinks;
    }

    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }


    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<LProductDTO> searchProducts(String keyword) {
        List<Product> matchingProducts = productRepository.findByNameContainingIgnoreCase(keyword);
        List<LProductDTO> lProductDTOs = new ArrayList<>();
        for (Product product : matchingProducts) {
            LProductDTO lProductDTO = new LProductDTO();
            lProductDTO.setId(product.getId());
            lProductDTO.setName(product.getName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setRate(product.getRate());
            lProductDTO.setConditions(product.getConditions());
            lProductDTO.setDateWarranty(product.getDateWarranty());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }
        return lProductDTOs;
    }

    @Override
    public DProductDTO getProductById(Long productId) {
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isEmpty()) {
            return null;
        }
        Product product = optionalProduct.get();
        List<SpecificationDetail> specificationDetails = specificationDetailRepository.findByProductId(productId);
        List<String> imageLinks = productImageRepository.findByProductId(productId)
                .stream()
                .map(ProductImage::getLink)
                .toList();

        DProductDTO dProductDTO = new DProductDTO();
        dProductDTO.setProduct(product);
        List<Map<String, Object>> specificationDetailDTOList = mapToSpecificationDetailDTOList(specificationDetails);
        dProductDTO.setSpecificationDetails(specificationDetailDTOList);
        dProductDTO.setImageLinks(new HashSet<>(imageLinks));
        return dProductDTO;
    }

    public List<Map<String, Object>> mapToSpecificationDetailDTOList(List<SpecificationDetail> specificationDetails) {
        Map<String, List<LSpecificationDetailDTO>> specificationDetailMap = new HashMap<>();

        for (SpecificationDetail specificationDetail : specificationDetails) {
            Specification specification = specificationDetail.getSpecification();
            String specificationName = specification.getSpecificationName();
            LSpecificationDetailDTO specificationDetailDTO = modelMapper.map(specificationDetail, LSpecificationDetailDTO.class);

            List<LSpecificationDetailDTO> specificationDetailList = specificationDetailMap.getOrDefault(specificationName, new ArrayList<>());
            specificationDetailList.add(specificationDetailDTO);
            specificationDetailMap.put(specificationName, specificationDetailList);
        }
        return specificationDetailMap.entrySet()
                .stream()
                .map(entry -> {
                    Map<String, Object> specificationDetailGroup = new HashMap<>();
                    specificationDetailGroup.put("name", entry.getKey());
                    specificationDetailGroup.put("details", entry.getValue());
                    return specificationDetailGroup;
                })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public UProductDTO updateProductInfo(UProductDTO updatedProductDto) {
        try {
            Product existingProduct = productRepository.findById(updatedProductDto.getId())
                    .orElseThrow(() -> new RuntimeException("Product not found"));
            updateProductFields(updatedProductDto, existingProduct);        // Update the product fields
            if (updatedProductDto.getFiles() != null && updatedProductDto.getFiles().length > 0 && !areAllFilesEmpty(updatedProductDto.getFiles())) {  // Check if new non-empty images were uploaded
                deleteOldProductImages(existingProduct.getId());          // Delete old product images from Firebase Storage
                saveProductImages(updatedProductDto.getFiles(), existingProduct);           // Save the new product images
            }
            Product savedProduct = productRepository.save(existingProduct); // Save the updated product
            UProductDTO responseDTO = modelMapper.map(savedProduct, UProductDTO.class); // Create the response DTO
            responseDTO.setImageLinks(getProductImageLinks(savedProduct));

            return responseDTO;
        } catch (NoSuchElementException e) {
            throw new RuntimeException("Product not found");
        }
    }

    private void updateProductFields(UProductDTO updatedProductDto, Product existingProduct) {
        existingProduct.setName(updatedProductDto.getName());
        existingProduct.setDescription(updatedProductDto.getDescription());
        existingProduct.setPrice(updatedProductDto.getPrice());
        existingProduct.setStock(updatedProductDto.getStock());
        existingProduct.setConditions(updatedProductDto.getConditions());
        existingProduct.setDiscount(updatedProductDto.getDiscount());
        existingProduct.setDateWarranty(updatedProductDto.getDateWarranty());
        existingProduct.setRate(updatedProductDto.getRate());
        // Update the categoryC if it is not null
        if (updatedProductDto.getCategoryC() != null) {
            CategoryC categoryC = categoryCRepository.findById(updatedProductDto.getCategoryC().getId())
                    .orElseThrow(() -> new RuntimeException("CategoryC not found"));
            existingProduct.setCategoryC(categoryC);
        }

        // Update the manufacturer if it is not null
        if (updatedProductDto.getManufacturer() != null) {
            Manufacturer manufacturer = manufacturerRepository.findById(updatedProductDto.getManufacturer().getId())
                    .orElseThrow(() -> new RuntimeException("Manufacturer not found"));
            existingProduct.setManufacturer(manufacturer);
        }
    }

    private boolean areAllFilesEmpty(MultipartFile[] files) {
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Transactional
    private void deleteOldProductImages(Long productId) {
        List<ProductImage> imageProducts = productImageRepository.findByProductId(productId);
        for (ProductImage imageProduct : imageProducts) {
            String imageUrl = imageProduct.getLink();
            String fileName = extractFilenameFromUrl(imageUrl);
            try {
                firebaseImageService.delete(fileName);
            } catch (IOException e) {
                e.printStackTrace();
                // Handle the image deletion error
            }
        }
        productImageRepository.deleteByProductId(productId);
    }

    @Transactional
    private void saveProductImages(MultipartFile[] imageFiles, Product savedProduct) {
        for (MultipartFile imageFile : imageFiles) {
            if (!imageFile.isEmpty()) {
                try {
                    String imageUrl = firebaseImageService.save(imageFile);
                    createProductImageEntity(savedProduct, imageUrl);
                } catch (IOException e) {
                    e.printStackTrace();
                    // Handle the file saving error
                }
            }
        }
    }

    private void createProductImageEntity(Product savedProduct, String imageUrl) {
        ProductImage productImage = new ProductImage();
        productImage.setLink(imageUrl);
        productImage.setProduct(savedProduct);
        productImageRepository.save(productImage);
    }


    @Override
    @Transactional
    public boolean deleteProduct(Long productId) {
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            List<Rate> rates = rateRepository.findAllByProductId_Id(productId);
            List<ProductImage> imageProducts = productImageRepository.findByProductId(productId);   // Delete product images from Firebase Storage
            for (ProductImage imageProduct : imageProducts) {
                String imageUrl = imageProduct.getLink();
                String fileName = extractFilenameFromUrl(imageUrl);
                try {
                    firebaseImageService.delete(fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            for (Rate rate : rates) {
                List<RateImage> rateImages = rateImageRepository.findAllByRateId_Id(rate.getId());
                for (RateImage rateImage : rateImages) {
                    String rateImageUrl = rateImage.getLink();
                    String rateImageFileName = extractFilenameFromUrl(rateImageUrl);
                    try {
                        firebaseImageService.delete(rateImageFileName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                rateImageRepository.deleteByRateId_Id(rate.getId());
            }
            rateRepository.deleteByProductId_Id(productId);
            commentRepository.deleteByProductId_Id(productId);
            productColorRepository.deleteByProductId(productId);
            productImageRepository.deleteByProductId(productId);  // Delete product images from the database
            specificationDetailRepository.deleteByProductId(productId);  // Delete specification details
            productRepository.deleteById(productId);         // Delete the product

            // Delete related Rate entities

            return true;
        }
        return false;
    }

    private String extractFilenameFromUrl(String imageUrl) {
        int startIndex = imageUrl.lastIndexOf("/") + 1;
        int endIndex = imageUrl.indexOf("?");
        return imageUrl.substring(startIndex, endIndex);
    }
    @Override
    public Page<LProductDTO> getAllProductDTOs(int page, Sort.Direction direction) {
        Pageable pageable = PageRequest.of(page, 10,Sort.by(direction,"id"));
        Page<Product> products = productRepository.findAll(pageable);

        List<LProductDTO> lProductDTOs = new ArrayList<>();
        for (Product product : products) {
            LProductDTO lProductDTO = new LProductDTO();
            lProductDTO.setId(product.getId());
            lProductDTO.setName(product.getName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setRate(product.getRate());
            lProductDTO.setDateWarranty(product.getDateWarranty());
            lProductDTO.setConditions(product.getConditions());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }

        return new PageImpl<>(lProductDTOs, pageable, products.getTotalElements());
    }


    @Override
    public Page<Product> getAllProducts(int page) {
        Pageable pageable = PageRequest.of(page, 10);
        return productRepository.findAll(pageable);
    }


}
