package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forCreate.DiscountPaymentDetailDTO;
import com.datmlt.didongvitbe.entity.DiscountPayment;
import com.datmlt.didongvitbe.entity.DiscountPaymentDetail;
import com.datmlt.didongvitbe.entity.Orders;
import com.datmlt.didongvitbe.repositories.DiscountPaymentDetailRepository;
import com.datmlt.didongvitbe.repositories.DiscountPaymentRepository;
import com.datmlt.didongvitbe.repositories.OrderRepository;
import com.datmlt.didongvitbe.service.DiscountPaymentDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountPaymentDetailServiceImpl implements DiscountPaymentDetailService {
    @Autowired
    private DiscountPaymentDetailRepository discountPaymentDetailRepository;
    @Autowired
    private DiscountPaymentRepository discountPaymentRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public DiscountPaymentDetail createDiscountPaymentDetail(DiscountPaymentDetailDTO discountPaymentDetailDTO) {
        try{
            DiscountPaymentDetail discountPaymentDetail = mapDiscountPaymentDetailDTOToEntity(discountPaymentDetailDTO);
            discountPaymentDetailRepository.save(discountPaymentDetail);


            return discountPaymentDetail;
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to add discount payment detail");
        }
    }
    private DiscountPaymentDetail mapDiscountPaymentDetailDTOToEntity(DiscountPaymentDetailDTO discountPaymentDetailDTO) {
        DiscountPaymentDetail discountPaymentDetail = new DiscountPaymentDetail();

        Orders orders = orderRepository.findById(discountPaymentDetailDTO.getOrderId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid order ID"));
        discountPaymentDetail.setOrderId(orders);

        DiscountPayment discountPayment = discountPaymentRepository.findById(discountPaymentDetailDTO.getDiscountPaymentId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid discount payment ID"));
        discountPaymentDetail.setDiscountPaymentId(discountPayment);

        return discountPaymentDetail;
    }
    @Override
    public List<DiscountPaymentDetail> getAllDiscountPaymentDetail(){
        return discountPaymentDetailRepository.findAll();
    }

    public boolean deleteAllPaymentDetail(){
        try{
            discountPaymentDetailRepository.deleteAll();
            return true;
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to delete all discount payment detail");
        }
    }

    /*@Override
    public RCheckDiscount existByOrderId(CheckDiscount id){
        RCheckDiscount rCheckDiscount = new RCheckDiscount();
        Optional<DiscountPaymentDetail> discountPaymentDetail = discountPaymentDetailRepository.findByOrderId(id.getOrderId().getId());
        if(discountPaymentDetailRepository.existsByOrderId(id.getOrderId().getId()) == true){
            rCheckDiscount.setPercent(discountPaymentDetail.get().getDiscountPaymentId().getPercent());
            return rCheckDiscount;
        }
        return rCheckDiscount;
    }*/
}
