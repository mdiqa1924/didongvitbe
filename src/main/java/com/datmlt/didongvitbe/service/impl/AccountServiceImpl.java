package com.datmlt.didongvitbe.service.impl;


import com.datmlt.didongvitbe.dto.AccountDTO;
import com.datmlt.didongvitbe.dto.RegisterModel;
import com.datmlt.didongvitbe.dto.UserDTO;
import com.datmlt.didongvitbe.entity.Account;
import com.datmlt.didongvitbe.entity.Role;
import com.datmlt.didongvitbe.repositories.AccountRepository;
import com.datmlt.didongvitbe.repositories.RoleRepository;
import com.datmlt.didongvitbe.security.jwt.JwtAuthResponse;
import com.datmlt.didongvitbe.security.jwt.JwtUtil;
import com.datmlt.didongvitbe.service.AccountService;
import com.datmlt.didongvitbe.service.CustomAccountDetails;
import com.datmlt.didongvitbe.service.UserService;
import com.datmlt.didongvitbe.utils.VerificationCodeManager;
import com.datmlt.didongvitbe.utils.constant.AuthProvider;
import com.datmlt.didongvitbe.utils.constant.AuthoritiesConstants;
import com.datmlt.didongvitbe.utils.exception.ApiException;
import jakarta.transaction.Transactional;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private RoleRepository roleRepository;

    @Value("${spring.mail.username")
    private String sender;


    @Override
    public Optional<Account> getByEmail(String email){
        return accountRepository.findByEmail(email);
    }



    @Override
    public JwtAuthResponse validateLogin(String email, String password) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(email, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = "Bearer " + jwtUtil.generateJwtToken(authentication);
        Account account = getByEmail(email).orElseThrow();
        UserDTO.Response userResponse = userService.getResponseUserByAccountId(account.getId());

        CustomAccountDetails userDetails = (CustomAccountDetails) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        AccountDTO.Response accountResponse = new AccountDTO.Response();
        accountResponse.setId(account.getId());
        accountResponse.setEmail(account.getEmail());
        accountResponse.setStatus(account.getStatus());
        accountResponse.setRole(roles.toString());
        accountResponse.setProvider(account.getProvider());
        accountResponse.setProviderId(account.getProviderId());
        accountResponse.setUser(userResponse);
        return new JwtAuthResponse(jwt, accountResponse);
    }


    @Override
    public Account create(RegisterModel registerModel) {
        accountRepository.findByEmail(registerModel.getEmail())
                .ifPresent(user -> {
                    throw new ApiException("Email Already exists.", HttpStatus.BAD_REQUEST);
                });

        Optional<Role> roles = roleRepository.findByName(AuthoritiesConstants.ROLE_CUSTOMER);
        Set<Role> authorities = new HashSet<>();
        Account newAccount = new Account();
        newAccount.setEmail(registerModel.getEmail());
        newAccount.setPassword(passwordEncoder.encode(registerModel.getPassword()));
        newAccount.setProvider(AuthProvider.local);
        roles.ifPresent(auth -> authorities.add(auth));
        newAccount.setRoles(authorities);
        newAccount.setStatus("ACTIVE");
        Account result = accountRepository.save(newAccount);
        return result;
    }


    @Override
    public void delete(Long id) {
        accountRepository.deleteById(id);
    }

    @Override
    public void changeRole(Long id) {
        Account account = accountRepository.findById(id).orElseThrow();
        Set<Role> currentAuthorities = account.getRoles();

        if(currentAuthorities.contains(roleRepository.findByName(AuthoritiesConstants.ROLE_CUSTOMER).orElse(null))){
            currentAuthorities.clear();
            currentAuthorities.add(roleRepository.findByName(AuthoritiesConstants.ROLE_MANAGER).orElse(null));
        }
        else {
            currentAuthorities.clear();
            currentAuthorities.add(roleRepository.findByName(AuthoritiesConstants.ROLE_CUSTOMER).orElse(null));
        }
        accountRepository.save(account);
    }

    @Override
    public List<Account> getAllAccount() {
        List<Account> accounts = accountRepository.findAll();
        return accounts;
    }

    @Override
    public List<Account> getByRole(String role) {
        List<Account> accounts = accountRepository.findAll();
        List<Account> accountsByRole = new ArrayList<>();
        for(Account account:accounts){
            if(account.getRoles().equals(role)){
                accountsByRole.add(account);
            }
        }
        return accountsByRole;
    }

    @Override
    public void changeStatus(Long id) {
        Account account = accountRepository.findById(id).get();
        String currentStatus = account.getStatus();
        if(currentStatus.equals("Active")){
            account.setStatus("Inactive");
        }
        else {
            account.setStatus("Active");
        }
        accountRepository.save(account);
    }

    @Override
    @Transactional
    public Account changePassword(AccountDTO.ChangePassword changePassword) {
        Optional<Account> accountOptional = accountRepository.findByEmail(changePassword.getEmail());
        if(accountOptional == null){
            throw new RuntimeException("Email doesn't exist.");
        }
        Account account = accountOptional.get();
        if(!passwordEncoder.matches(changePassword.getCurrentPassword(), account.getPassword())){
            throw new RuntimeException("Password doesn't match.");
        }
        account.setPassword(passwordEncoder.encode(changePassword.getNewPassword()));
        Account result = accountRepository.save(account);
        return result;
    }

    @Override
    public AccountDTO.EmailResult sendResetPassword(AccountDTO.ChangePassword changePassword) {
        Optional<Account> accountOptional = accountRepository.findByEmail(changePassword.getEmail());
        if(accountOptional == null){
            throw new RuntimeException("Email doesn't exist.");
        }
        Account account = accountOptional.get();
        String verificationCode = generateVerificationCode();
        VerificationCodeManager.saveVerificationCode(changePassword.getEmail(), verificationCode);
        sendEmail(changePassword.getEmail(), "Reset Password","Your verification code is: " + verificationCode);
        return new AccountDTO.EmailResult(true, "Send reset email successfully!");
    }

    @Override
    @Transactional
    public Account resetPassword(AccountDTO.ChangePassword changePassword) {
        boolean isCodeValid = VerificationCodeManager.isVerificationCodeValid(changePassword.getEmail(), changePassword.getVerificationCode());
        if(isCodeValid){
            try{
                Optional<Account> accountOptional = accountRepository.findByEmail(changePassword.getEmail());
                if(accountOptional == null){
                    throw new RuntimeException("Email doesn't exist.");
                }
                Account account = accountOptional.get();
                account.setPassword(passwordEncoder.encode(changePassword.getNewPassword()));
                accountRepository.save(account);
                sendEmail(changePassword.getEmail(), "Reset password successfully!", "Your password has been reset.");
                VerificationCodeManager.removeVerificationCode(changePassword.getEmail());
                return account;
            }catch (Exception e){
                e.printStackTrace();
                throw new ServiceException("Error occurs when resetting password.");
            }
        }
        else {
            throw new RuntimeException("Verification Code is not valid.");
        }
    }

    private String generateVerificationCode(){
        Integer codeLength = 6;
        String allowedChars = "0123456789";
        Random random = new Random();

        System.out.println("allowChars length: " + allowedChars.length());

        StringBuilder verificationCode = new StringBuilder(codeLength);
        for(int i = 0; i<codeLength; i++){
            int randomIndex = random.nextInt(allowedChars.length());
            char randomChar = allowedChars.charAt(randomIndex);
            verificationCode.append(randomChar);
        }
        return verificationCode.toString();
    }

    public AccountDTO.EmailResult sendEmail(String email, String subject, String content){
        try{
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(sender);
            message.setTo(email);
            message.setSubject(subject);
            message.setText(content);
            javaMailSender.send(message);
            return new AccountDTO.EmailResult(true, "Email");
        }
        catch (Exception e){
            e.printStackTrace();
            return new AccountDTO.EmailResult(false, "Fail to send email: " + e.getMessage());
        }
    }
}
