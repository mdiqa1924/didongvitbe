package com.datmlt.didongvitbe.service.impl;


import com.datmlt.didongvitbe.dto.forList.LProductDTO;
import com.datmlt.didongvitbe.entity.CategoryC;
import com.datmlt.didongvitbe.entity.Product;
import com.datmlt.didongvitbe.entity.ProductImage;
import com.datmlt.didongvitbe.repositories.CategoryCRepository;
import com.datmlt.didongvitbe.repositories.ProductImageRepository;
import com.datmlt.didongvitbe.repositories.ProductRepository;
import com.datmlt.didongvitbe.service.CategoryCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryCServiceImpl implements CategoryCService {

    @Autowired
    private CategoryCRepository categoryCRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductImageRepository productImageRepository;

    @Override
    public CategoryC createCategoryC(CategoryC categoryC) {
        return categoryCRepository.save(categoryC);
    }

    @Override
    public List<CategoryC> getAllCategoriesC() {
        return categoryCRepository.findAll();
    }

    @Override
    public Optional<CategoryC> getCategoryCById(Long categoryId) {
        return categoryCRepository.findById(categoryId);
    }

    @Override
    public CategoryC updateCategoryC(CategoryC category) {
        return categoryCRepository.save(category);
    }

    @Override
    public boolean deleteCategoryCById(Long categoryId) {
        if (categoryCRepository.existsById(categoryId)) {
            categoryCRepository.deleteById(categoryId);
            return true;
        }
        return false;
    }

    @Override
    public List<LProductDTO> getProductsByCategoryC(CategoryC categoryC) {
        List<Product> products = productRepository.findByCategoryC(categoryC);
        List<LProductDTO> lProductDTOs = new ArrayList<>();
        for (Product product : products) {
            LProductDTO lProductDTO = new LProductDTO();
            lProductDTO.setId(product.getId());
            lProductDTO.setName(product.getName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setRate(product.getRate());
            lProductDTO.setConditions(product.getConditions());
            lProductDTO.setDateWarranty(product.getDateWarranty());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }
        return lProductDTOs;
    }
    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }
}

