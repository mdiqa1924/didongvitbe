package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forCreate.CRequestVoucherDetail;
import com.datmlt.didongvitbe.entity.VoucherDetail;
import com.datmlt.didongvitbe.repositories.VoucherDetailRepository;
import com.datmlt.didongvitbe.service.VoucherDetailService;
import com.datmlt.didongvitbe.service.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoucherDetailServiceImpl implements VoucherDetailService {
    @Autowired
    VoucherDetailRepository voucherDetailRepository;
    @Autowired
    VoucherService voucherService;
    @Override
    public VoucherDetail insert(CRequestVoucherDetail cRequestVoucherDetail) {
        VoucherDetail voucherDetail = new VoucherDetail(cRequestVoucherDetail.getOrdersID(), voucherService.check(cRequestVoucherDetail.getVoucherCode()));
        return voucherDetailRepository.save(voucherDetail);
    }
}
