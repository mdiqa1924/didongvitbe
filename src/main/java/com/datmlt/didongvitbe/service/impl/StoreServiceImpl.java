package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.entity.Store;
import com.datmlt.didongvitbe.repositories.StoreRepository;
import com.datmlt.didongvitbe.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private StoreRepository storeRepository;

    @Override
    public Store createStore(Store store) {
        return storeRepository.save(store);
    }

    @Override
    public List<Store> getAllStores() {
        return storeRepository.findAll();
    }

    @Override
    public Optional<Store> getStoreById(Long storeId) {
        return storeRepository.findById(storeId);
    }

    @Override
    public Store updateStore(Store store) {
        return storeRepository.save(store);
    }

    @Override
    public boolean deleteStoreById(Long storeId) {
        if (storeRepository.existsById(storeId)) {
            storeRepository.deleteById(storeId);
            return true;
        }
        return false;
    }
    @Override
    public List<Store>findByName(String query){
        List<Store> stores=storeRepository.findAllByStoreNameContains(query);
        return stores;
    }
    @Override
    public List<Store> findBy2Contains(String query1,String query2){
        List<Store> stores=storeRepository.findAllByStoreNameContainsAndStoreNameContaining(query1,query2);
        return stores;
    }
}

