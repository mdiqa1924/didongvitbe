package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forList.LProductManufacturer;
import com.datmlt.didongvitbe.entity.Manufacturer;
import com.datmlt.didongvitbe.entity.Product;
import com.datmlt.didongvitbe.entity.ProductImage;
import com.datmlt.didongvitbe.repositories.ManufacturerRepository;
import com.datmlt.didongvitbe.repositories.ProductImageRepository;
import com.datmlt.didongvitbe.repositories.ProductRepository;
import com.datmlt.didongvitbe.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductImageRepository productImageRepository;
    private ManufacturerRepository manufacturerRepository;
    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository){
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    @Transactional
    public Manufacturer createManufacturer(Manufacturer manufacturer){
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    @Transactional
    public Manufacturer updateManufacturer(Manufacturer manufacturer){
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    @Transactional
    public Boolean deleteManufacturer(Long id){
        if(manufacturerRepository.existsById(id)){
            manufacturerRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<Manufacturer> getAllManufacturer()
    {
        return manufacturerRepository.findAll();
    }

    @Override
    public List<LProductManufacturer> getProductsByManufacturer(Manufacturer manufacturer) {
        List<Product> products = productRepository.findByManufacturer(manufacturer);
        List<LProductManufacturer> lProductDTOs = new ArrayList<>();
        for (Product product : products) {
            LProductManufacturer lProductDTO = new LProductManufacturer();
            lProductDTO.setId(product.getId());
            lProductDTO.setName(product.getName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setRate(product.getRate());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }
        return lProductDTOs;
    }
    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }

}
