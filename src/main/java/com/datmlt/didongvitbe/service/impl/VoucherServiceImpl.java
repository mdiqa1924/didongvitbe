package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forCreate.CRequestVoucher;
import com.datmlt.didongvitbe.dto.forList.RLResponseVoucher;
import com.datmlt.didongvitbe.entity.Voucher;
import com.datmlt.didongvitbe.repositories.VoucherRepository;
import com.datmlt.didongvitbe.service.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class VoucherServiceImpl implements VoucherService {
    @Autowired
    VoucherRepository voucherRepository;
    @Override
    public Voucher insert(CRequestVoucher cRequestVoucher) {
        Voucher voucher = new Voucher();
        voucher.setVoucherCode(cRequestVoucher.getVoucherCode());
        voucher.setVoucherName(cRequestVoucher.getVoucherName());
        voucher.setConditions(cRequestVoucher.getCondition());
        voucher.setDiscountValue(cRequestVoucher.getDiscountValue());
        voucher.setQuantity(cRequestVoucher.getQuantity());
        voucher.setStock(cRequestVoucher.getStock());
        voucher.setDateStart(cRequestVoucher.getDateStart());
        voucher.setDateEnd(cRequestVoucher.getDateEnd());
        return voucherRepository.save(voucher);
    }

    @Override
    public Voucher getOne(Long id) {
        return voucherRepository.findById(id).get();
    }

    @Override
    public List<RLResponseVoucher> getALl() {
        List<Voucher> voucherList = new ArrayList<Voucher>(voucherRepository.findAll());
        List<RLResponseVoucher> RLResponseVouchersList = new ArrayList<RLResponseVoucher>();
        for(int i=0; i<voucherList.size(); i++){
            Voucher a = voucherList.get(i);
            RLResponseVoucher RLResponseVouchers = new RLResponseVoucher();
            RLResponseVouchers.setId(a.getId());
            RLResponseVouchers.setVoucherName(a.getVoucherName());
            RLResponseVouchers.setVoucherCode(a.getVoucherCode());
            RLResponseVouchers.setDiscountValue(a.getDiscountValue());
            RLResponseVouchers.setDateStart(a.getDateStart());
            RLResponseVouchers.setDateEnd(a.getDateEnd());
            RLResponseVouchersList.add(RLResponseVouchers);
        }
        return RLResponseVouchersList;
    }

    @Override
    @Transactional
    public Voucher update(Voucher voucher) {
        return voucherRepository.save(voucher);
    }

    @Override
    public List<RLResponseVoucher> search(String s) {
        List<Voucher> voucherList = new ArrayList<Voucher>(voucherRepository.findAllByVoucherNameContains(s));
        List<RLResponseVoucher> RLResponseVouchersList = new ArrayList<RLResponseVoucher>();
        for(int i=0; i<voucherList.size(); i++){
            Voucher a = voucherList.get(i);
            RLResponseVoucher RLResponseVouchers = new RLResponseVoucher();
            RLResponseVouchers.setId(a.getId());
            RLResponseVouchers.setVoucherName(a.getVoucherName());
            RLResponseVouchers.setVoucherCode(a.getVoucherCode());
            RLResponseVouchers.setDiscountValue(a.getDiscountValue());
            RLResponseVouchers.setDateStart(a.getDateStart());
            RLResponseVouchers.setDateEnd(a.getDateEnd());
            RLResponseVouchersList.add(RLResponseVouchers);
        }
        return RLResponseVouchersList;
    }

    @Override
    public Voucher check(String code) {
        return voucherRepository.findByVoucherCode(code);
    }

}
