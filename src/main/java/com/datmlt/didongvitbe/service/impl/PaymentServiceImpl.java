package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.entity.Payment;
import com.datmlt.didongvitbe.repositories.PaymentRepository;
import com.datmlt.didongvitbe.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public Payment createPayment(Payment payment){
        try{
            return paymentRepository.save(payment);
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to add payment");
        }
    }
    @Override
    public List<Payment> getAllPayment(){
        return paymentRepository.findAll();
    }
    @Override
    public Optional<Payment> getPaymentById(Long id){

        return paymentRepository.findById(id);
    }
    @Override
    public Payment updatePayment(Payment payment){
        return paymentRepository.save(payment);
    }
    @Override
    public boolean deletePaymentByTd(Long id){
        if(paymentRepository.existsById(id)){
            paymentRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
