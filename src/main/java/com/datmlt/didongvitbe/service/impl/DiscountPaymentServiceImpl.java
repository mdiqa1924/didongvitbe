package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forCreate.DiscountPaymentDTO;
import com.datmlt.didongvitbe.dto.forResponse.RDiscountPaymentDTO;
import com.datmlt.didongvitbe.entity.DiscountPayment;
import com.datmlt.didongvitbe.entity.Payment;
import com.datmlt.didongvitbe.repositories.DiscountPaymentRepository;
import com.datmlt.didongvitbe.service.DiscountPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DiscountPaymentServiceImpl implements DiscountPaymentService {
    @Autowired
    private DiscountPaymentRepository discountPaymentRepository;

    @Override
    public RDiscountPaymentDTO createDiscountPayment(DiscountPaymentDTO discountPaymentDTO) {
        try{
            DiscountPayment discountPayment = mapDiscountPaymentDTOToEntity(discountPaymentDTO);
            discountPaymentRepository.save(discountPayment);

            RDiscountPaymentDTO rDiscountPaymentDTO = setupResponse(discountPayment);
            return rDiscountPaymentDTO;
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to add discount payment");
        }
    }
    private DiscountPayment mapDiscountPaymentDTOToEntity(DiscountPaymentDTO discountPaymentDTO){
        DiscountPayment discountPayment = new DiscountPayment();
        if(discountPaymentDTO.getId() != null){
            discountPayment.setId(discountPaymentDTO.getId());
        }
        discountPayment.setDiscountPaymentName(discountPaymentDTO.getDiscountPaymentName());
        discountPayment.setPercent(discountPaymentDTO.getPercent());
        discountPayment.setStatus(discountPaymentDTO.getStatus());

        Payment payment = new Payment();
        payment.setId(discountPaymentDTO.getPaymentId());
        discountPayment.setPaymentId(payment);
        return discountPayment;
    }
    private RDiscountPaymentDTO setupResponse(DiscountPayment discountPayment){
        RDiscountPaymentDTO rDiscountPaymentDTO = new RDiscountPaymentDTO();
        rDiscountPaymentDTO.setId(discountPayment.getId());
        rDiscountPaymentDTO.setDiscountPaymentName(discountPayment.getDiscountPaymentName());
        rDiscountPaymentDTO.setPercent(discountPayment.getPercent());
        rDiscountPaymentDTO.setStatus(discountPayment.getStatus());
        rDiscountPaymentDTO.setPaymentId(discountPayment.getPaymentId().getId());
        return rDiscountPaymentDTO;
    }
    @Override
    public List<DiscountPayment> getAllDiscountPayment(){
        return discountPaymentRepository.findAll();
    }
    @Override
    public Optional<DiscountPayment> getDiscountPaymentById(Long id){
        return discountPaymentRepository.findById(id);
    }
    @Override
    public RDiscountPaymentDTO updateDiscountPayment(DiscountPaymentDTO discountPaymentDTO){
        try{
            DiscountPayment discountPayment = mapDiscountPaymentDTOToEntity(discountPaymentDTO);
            discountPaymentRepository.save(discountPayment);

            RDiscountPaymentDTO rDiscountPaymentDTO = setupResponse(discountPayment);
            return rDiscountPaymentDTO;
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to update discount payment");
        }
    }
    @Override
    public boolean deleteDiscountPaymentByTd(Long id){
        if(discountPaymentRepository.existsById(id)){
            discountPaymentRepository.deleteById(id);
            return true;
        }
        return false;
    }
    @Override
    public List<DiscountPayment> findByDiscountPaymentNameContaining(String name) {
        return discountPaymentRepository.findByDiscountPaymentNameContaining(name);
    }
}
