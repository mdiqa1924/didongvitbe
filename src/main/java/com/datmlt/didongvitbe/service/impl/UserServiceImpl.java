package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.*;
import com.datmlt.didongvitbe.entity.*;
import com.datmlt.didongvitbe.repositories.*;
import com.datmlt.didongvitbe.service.*;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private FirebaseImageService firebaseImageService;

    @Override
    public User create(RegisterModel registerModel) {
        User newUser = new User();
        newUser.setFullName(registerModel.getFullName());
        newUser.setPhoneNumber(registerModel.getPhoneNumber());
        newUser.setCreateAt(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        Optional<Account> accountOptional = accountRepository.findByEmail(registerModel.getEmail());
        Account account = accountOptional.get();
        newUser.setAccount(account);
        User result = userRepository.save(newUser);

        Cart newCart = new Cart();
        newCart.setUserId(newUser);
        cartRepository.save(newCart);

        return result;
    }

    @Override
    @Transactional
    public User update(MultipartFile multipartFile, Long id, UserDTO.Update updateUser){
        try {
            User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
            updateUserField(updateUser, user);
            if (multipartFile != null && !areAllFilesEmpty(multipartFile)){
                deleteOldAvatar(user.getId());
                savedAvatar(multipartFile, user);
            }
            User result = userRepository.save(user);
            String imageUrl = result.getAvatar();
            return result;

        } catch (NoSuchElementException e) {
            throw new RuntimeException("Product not found");
        }
    }
    private boolean areAllFilesEmpty(MultipartFile file) {
            if (!file.isEmpty()) {
                return false;
            }
        return true;
    }
    private void deleteOldAvatar(Long userId) {
        String imageUrl = userRepository.findById(userId)
                .map(User::getAvatar)
                .orElse(null);
        if (imageUrl != null) {
            String fileName = extractFilenameFromUrl(imageUrl);
            try {
                firebaseImageService.delete(fileName);
            } catch (IOException e) {
                e.printStackTrace();
                // Handle the image deletion error
            }
        }
    }
    private String extractFilenameFromUrl(String imageUrl) {
        int startIndex = imageUrl.lastIndexOf("/") + 1;
        int endIndex = imageUrl.indexOf("?");
        return imageUrl.substring(startIndex, endIndex);
    }

    private User updateUserField(UserDTO.Update updateUser, User existingUser) {
        existingUser.setFullName(updateUser.getFullName());
        existingUser.setPhoneNumber(updateUser.getPhoneNumber());
        existingUser.setDateOfBirth(updateUser.getDateOfBirth());
        return existingUser;
    }

    private void savedAvatar(MultipartFile multipartFile, User savedAvatar) {
            if (!multipartFile.isEmpty()) {
                try {
                    String imageUrl = firebaseImageService.save(multipartFile);
                    savedAvatar.setAvatar(imageUrl);
                } catch (IOException e) {
                    e.printStackTrace();
                    // Handle the file saving error
                }
            }
    }


    @Override
    public List<User> getAll() {
        List<User> users = userRepository.findAllBy();
        return users;
    }

    @Override
    public UserDTO.Response getResponseUserByUserId(Long id) {
        User user = userRepository.findById(id).get();
        UserDTO.Response responseUser = new UserDTO.Response();
        responseUser.setId(user.getId());
        responseUser.setFullName(user.getFullName());
        responseUser.setPhoneNumber(user.getPhoneNumber());
        responseUser.setDateOfBirth(user.getDateOfBirth());
        responseUser.setAvatar(user.getAvatar());
        responseUser.setCreateAt(user.getCreateAt());
        return responseUser;
    }

    @Override
    public UserDTO.Response getResponseUserByAccountId(Long id) {
        User user = userRepository.findByAccount_Id(id);
        UserDTO.Response userDtoResponse = new UserDTO.Response();
        userDtoResponse.setId(user.getId());
        userDtoResponse.setFullName(user.getFullName());
        userDtoResponse.setAvatar(user.getAvatar());
        userDtoResponse.setDateOfBirth(user.getDateOfBirth());
        userDtoResponse.setPhoneNumber(user.getPhoneNumber());
        userDtoResponse.setCreateAt(user.getCreateAt());
        userDtoResponse.setAccountId(id);
        return userDtoResponse;
    }

    @Override
    public User getUserByAccountId(Long id) {
        return userRepository.findByAccount_Id(id);
    }

    @Override
    public User getUserByUserId(Long id) {
        return userRepository.findById(id).get();
    }
}


