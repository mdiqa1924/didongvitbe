package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forCreate.CommentDTO;
import com.datmlt.didongvitbe.entity.Comment;
import com.datmlt.didongvitbe.entity.Product;
import com.datmlt.didongvitbe.repositories.CommentRepository;
import com.datmlt.didongvitbe.repositories.ProductRepository;
import com.datmlt.didongvitbe.service.CommentService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;

    private final ProductRepository productRepository;

    public CommentServiceImpl( CommentRepository commentRepository, ProductRepository productRepository){
        this.commentRepository = commentRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<CommentDTO> getCommentsByProductId(Long productId) {
        List<Comment> comments = commentRepository.findAllByProductId(productId);
        List<CommentDTO> commentDTOS = new ArrayList<>();
        for (Comment comment : comments){
            CommentDTO commentDTO = new CommentDTO();
            commentDTO.setPhoneNumber(comment.getPhoneNumber());
            commentDTO.setFullName(comment.getFullName());
            commentDTO.setTime(comment.getTime());
            commentDTO.setContent(comment.getContent());
            commentDTO.setProductId(comment.getProductId().getId());
            commentDTOS.add(commentDTO);
        }
        return commentDTOS;
    }


    @Override
    public CommentDTO createComment(CommentDTO commentDTO) {
        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedTime = currentTime.format(formatter);

        Comment comment = new Comment();
        comment.setPhoneNumber(commentDTO.getPhoneNumber());
        comment.setFullName(commentDTO.getFullName());
        comment.setContent(commentDTO.getContent());
        comment.setTime(formattedTime);
        Product product = productRepository.findById(commentDTO.getProductId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid Product Id"));
        comment.setProductId(product);

        Comment createdComment = commentRepository.save(comment);

        CommentDTO createdCommentDTO = new CommentDTO();
        createdCommentDTO.setPhoneNumber(createdCommentDTO.getPhoneNumber());
        createdCommentDTO.setFullName(createdCommentDTO.getFullName());
        createdCommentDTO.setContent(createdComment.getContent());
        createdCommentDTO.setTime(createdComment.getTime());
        createdCommentDTO.setProductId(createdCommentDTO.getProductId());

        return createdCommentDTO;
    }


    @Override
    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }
}
