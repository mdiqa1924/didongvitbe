package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forCreate.CRequestDiscountCombo;
import com.datmlt.didongvitbe.entity.DiscountCombo;
import com.datmlt.didongvitbe.repositories.DiscountComboRepository;
import com.datmlt.didongvitbe.service.DiscountComboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountComboServiceImpl implements DiscountComboService {
    @Autowired
    DiscountComboRepository discountComboRepository;
    @Override
    public DiscountCombo insert(CRequestDiscountCombo cRequestDiscountCombo) {
        DiscountCombo discountCombo = new DiscountCombo();
        discountCombo.setDiscountName(cRequestDiscountCombo.getDiscountName());
        discountCombo.setPercent(cRequestDiscountCombo.getPercent());
        discountCombo.setDateStart(cRequestDiscountCombo.getDateStart());
        discountCombo.setDateEnd(cRequestDiscountCombo.getDateEnd());
        return discountComboRepository.save(discountCombo);
    }

    @Override
    public DiscountCombo getOne(Long id) {
        return discountComboRepository.findById(id).get();
    }

    @Override
    public List<DiscountCombo> getAll() {
        List<DiscountCombo> discountComboList = discountComboRepository.findAll();
        return discountComboList;
    }

    @Override
    public DiscountCombo update(DiscountCombo discountCombo) {
        return discountComboRepository.save(discountCombo);
    }

    @Override
    public List<DiscountCombo> search(String s) {
        return discountComboRepository.findAllByDiscountNameContains(s);
    }
}

