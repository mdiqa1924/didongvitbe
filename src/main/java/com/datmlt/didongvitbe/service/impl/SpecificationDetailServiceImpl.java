package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forCreate.CSpecificationDetailDTO;
import com.datmlt.didongvitbe.dto.forList.LSpecificationDetailDTO;
import com.datmlt.didongvitbe.dto.forUpdate.USpecificationDetailDTO;
import com.datmlt.didongvitbe.entity.*;
import com.datmlt.didongvitbe.repositories.*;
import com.datmlt.didongvitbe.service.SpecificationDetailService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SpecificationDetailServiceImpl implements SpecificationDetailService {
    @Autowired
    private SpecificationDetailRepository specificationDetailRepository;
    @Autowired
    private SpecificationRepository specificationRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public List<CSpecificationDetailDTO> saveSpecificationDetails(Long productId, List<CSpecificationDetailDTO> cSpecificationDetailDTOList) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new IllegalArgumentException("Product not found with ID: " + productId));
        List<SpecificationDetail> savedSpecificationDetails = cSpecificationDetailDTOList.stream().map(cSpecificationDetailDTO -> {
            Specification specification = specificationRepository.findById(cSpecificationDetailDTO.getId()).orElseThrow(() ->
                    new IllegalArgumentException("Specification not found with ID: " + cSpecificationDetailDTO.getId()));

            if (specificationDetailRepository.existsByProductAndNameDetail(product, cSpecificationDetailDTO.getNameDetail())) {
                throw new IllegalArgumentException("Specification detail already exists for the provided product and name detail");
            }
            SpecificationDetail specificationDetail = new SpecificationDetail();
            specificationDetail.setProduct(product);
            specificationDetail.setSpecification(specification);
            specificationDetail.setNameDetail(cSpecificationDetailDTO.getNameDetail());
            specificationDetail.setDescription(cSpecificationDetailDTO.getDescription());
            return specificationDetailRepository.save(specificationDetail);
        }).collect(Collectors.toList());
        return savedSpecificationDetails.stream().map(savedSpecificationDetail -> new CSpecificationDetailDTO(savedSpecificationDetail.getId(),
                savedSpecificationDetail.getSpecification().getSpecificationName(), savedSpecificationDetail.getNameDetail(),
                savedSpecificationDetail.getDescription(), savedSpecificationDetail.getProduct().getId())).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public boolean updateSpecificationDetailByProductId(Long productId, List<USpecificationDetailDTO> specificationDetailDTOs) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Product not found with ID: " + productId));
        List<SpecificationDetail> specificationDetails = specificationDetailRepository.findByProductId(productId);
        // Iterate over the updated specification details
        for (USpecificationDetailDTO specificationDetailDTO : specificationDetailDTOs) {
            for (SpecificationDetail specificationDetail : specificationDetails) {
                // Find the matching specification detail
                if (specificationDetail.getId() == (specificationDetailDTO.getId())) {
                    // Update the specification detail properties
                    specificationDetail.setNameDetail(specificationDetailDTO.getNameDetail());
                    specificationDetail.setDescription(specificationDetailDTO.getDescription());
                    specificationDetailRepository.save(specificationDetail); // Save the updated specification detail
                    break;
                }
            }
        }

        return true; // Return true - successful update
    }


    @Override
    public List<Map<String, Object>> getListDetailById(Long productId) {
        List<SpecificationDetail> specificationDetails = specificationDetailRepository.findByProductId(productId);

        List<Map<String, Object>> specificationDetailDTOList = mapToSpecificationDetailDTOList(specificationDetails);

        return specificationDetailDTOList;
    }
    private List<Map<String, Object>> mapToSpecificationDetailDTOList(List<SpecificationDetail> specificationDetails) {
        Map<String, List<LSpecificationDetailDTO>> specificationDetailMap = new HashMap<>();

        for (SpecificationDetail specificationDetail : specificationDetails) {
            Specification specification = specificationDetail.getSpecification();
            String specificationName = specification.getSpecificationName();
            LSpecificationDetailDTO specificationDetailDTO = modelMapper.map(specificationDetail, LSpecificationDetailDTO.class);

            List<LSpecificationDetailDTO> specificationDetailList = specificationDetailMap.getOrDefault(specificationName, new ArrayList<>());
            specificationDetailList.add(specificationDetailDTO);
            specificationDetailMap.put(specificationName, specificationDetailList);
        }
        return specificationDetailMap.entrySet().stream().map(entry -> {
                    Map<String, Object> specificationDetailGroup = new HashMap<>();
                    specificationDetailGroup.put("name", entry.getKey());
                    specificationDetailGroup.put("details", entry.getValue());
                    return specificationDetailGroup;
                }).collect(Collectors.toList());
    }


}
