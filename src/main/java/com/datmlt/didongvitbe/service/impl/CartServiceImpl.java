package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.dto.forDetail.CartDetailDTO;
import com.datmlt.didongvitbe.dto.forList.CartDTO;
import com.datmlt.didongvitbe.entity.*;
import com.datmlt.didongvitbe.repositories.*;
import com.datmlt.didongvitbe.service.CartService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CartDetailRepository cartDetailRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private  ProductImageRepository productImageRepository;
    @Autowired
    private  ProductColorRepository productColorRepository;

    public CartServiceImpl(CartRepository cartRepository, CartDetailRepository cartDetailRepository, ModelMapper modelMapper) {
        this.cartRepository = cartRepository;
        this.cartDetailRepository = cartDetailRepository;
        this.modelMapper = modelMapper;
    }
    public static class NotFoundException extends RuntimeException {
        public NotFoundException(String message) {
            super(message);
        }
    }
    @Override
    public CartDTO getCartById(Long userId) {
        Cart cart = cartRepository.findByUserId_Id(userId);
        Long cartId = cart.getId();
        //Cart cart1 = cartRepository.findById(cartId).orElse(null);
        if (cart == null) {
            throw new NotFoundException("Cart not found.");
        }
        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);
        List<CartDetailDTO> cartDetailDTOs = new ArrayList<>();
        Double total = 0.0;
        for (CartDetail cartDetail : cartDetails) {
            CartDetailDTO cartDetailDTO = convertToCartDetailDTO(cartDetail);
            cartDetailDTOs.add(cartDetailDTO);
            total += cartDetail.getUnitPrice()*cartDetail.getQuantity();
        }
        cart.setTotal(total);

        CartDTO cartDTO = new CartDTO();
        cartDTO.setUserId(userId);
        cartDTO.setCartDetailDTOList(cartDetailDTOs);

        return cartDTO;
    }

    @Override
    public CartDTO addProductToCart(Long userId, Long productId, Long colorId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User not found with ID: " + userId));

        // Check if the user already has a cart
        Cart cart = cartRepository.findByUserId_Id(userId);
        if (cart == null) {
            // Create a new cart for the user
            cart = new Cart();
            cart.setUserId(user);
            cart = cartRepository.save(cart);
        }

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Product not found with ID: " + productId));

        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);

        // Check if the product already exists in the cart
        CartDetail existingCartDetail = null;
        for (CartDetail cartDetail : cartDetails) {
            if (cartDetail.getProductId().equals(product)) {
                existingCartDetail = cartDetail;
                break;
            }
        }

        if (existingCartDetail != null) {
            // Increase the quantity of the existing cart detail
            existingCartDetail.setQuantity(existingCartDetail.getQuantity() + 1);
            cartDetailRepository.save(existingCartDetail);
        } else {
            CartDetail cartDetail = new CartDetail();
            cartDetail.setProductId(product);
            cartDetail.setQuantity(1);
            cartDetail.setUnitPrice(product.getDiscount());
            cartDetail.setPrice(product.getDiscount() * cartDetail.getQuantity());

            cartDetail.setCartId(cart);
            ProductColor color = productColorRepository.findByProduct_IdAndColor_Id(productId,colorId);
            cartDetail.setProductColor(color.getColor().getId());
            cartDetailRepository.save(cartDetail);
        }

        CartDTO cartDTO = new CartDTO();
        cartDTO.setUserId(userId);
        cartDTO.setCartDetailDTOList(getCartDetailsByCartId(cart.getId()));
        return cartDTO;
    }



    @Override
    public List<CartDetailDTO> getCartDetailsByCartId(Long userId) {

        Cart cart = cartRepository.findByUserId_Id(userId);
        Long cartId = cart.getId();
        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);

        List<CartDetailDTO> cartDetailDTOs = new ArrayList<>();
        for (CartDetail cartDetail : cartDetails) {
            CartDetailDTO cartDetailDTO = convertToCartDetailDTO(cartDetail);
            cartDetailDTOs.add(cartDetailDTO);
        }

        return cartDetailDTOs;
    }
    @Override
    public void clearCart(Long userId) {

        Cart cart = cartRepository.findByUserId_Id(userId);
        Long cartId = cart.getId();
        // Retrieve the cart details associated with the cart
        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);
        // Remove all cart details
        cartDetailRepository.deleteAll(cartDetails);
        // Reset the cart total to zero or any desired value
        cart.setTotal((double) 0L);
        // Save the updated cart
        cartRepository.save(cart);
    }



    @Override
    public void removeCartDetail(Long cartDetailId) {
        CartDetail cartDetail = cartDetailRepository.findById(cartDetailId)
                .orElseThrow(() -> new NotFoundException("Cart detail not found with ID: " + cartDetailId));

        // Retrieve the cart associated with the cart detail
        Cart cart = cartDetail.getCartId();

        // Retrieve the cart details from the repository based on the cart
        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);

        // Find the cart detail to be removed and remove it from the list
        cartDetails.removeIf(cd -> cd.getId().equals(cartDetailId));

        // Save the updated cart
        cartRepository.save(cart);

        // Delete the cart detail from the repository
        cartDetailRepository.delete(cartDetail);
    }



    public CartDetailDTO updateCartDetail(Long cartDetailId, CartDetailDTO cartDetailDTO,int quantityChange) {
        CartDetail existingCartDetail = cartDetailRepository.findById(cartDetailId).orElse(null);
        if (existingCartDetail != null) {
            int newQuantity = existingCartDetail.getQuantity() + quantityChange;
            if (newQuantity >= 1 && newQuantity <= 3) {
                existingCartDetail.setQuantity(newQuantity);
                CartDetail updatedCartDetail = cartDetailRepository.save(existingCartDetail);
                return convertToCartDetailDTO(updatedCartDetail);
            } else {
                throw new IllegalArgumentException("Invalid quantity. Quantity should be between 1 and 3.");
            }
        }
        return null;
    }
    @Override
    public List<CartDetailDTO> getAllCartDetails(Long userId) {
        Cart cart = cartRepository.findByUserId_Id(userId);
        Long cartId = cart.getId();
        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);

        List<CartDetailDTO> cartDetailDTOs = new ArrayList<>();
        for (CartDetail cartDetail : cartDetails) {
            CartDetailDTO cartDetailDTO = convertToCartDetailDTO(cartDetail);
            cartDetailDTOs.add(cartDetailDTO);
        }

        return cartDetailDTOs;
    }

    @Override
    public CartDTO getCartByUserId(Long userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new NotFoundException("User not found.");
        }
        Cart cart = cartRepository.findByUserId_Id(userId);

        return convertCartDetailsToDTO(cart);
    }

    private CartDTO convertCartDetailsToDTO(Cart cart) {
        List<CartDetail> cartDetails = cartDetailRepository.findByCartId(cart);
        List<CartDetailDTO> cartDetailDTOs = new ArrayList<>();

        for (CartDetail cartDetail : cartDetails) {
            CartDetailDTO cartDetailDTO = convertToCartDetailDTO(cartDetail);
            cartDetailDTOs.add(cartDetailDTO);
        }
        User user =  new User();
        CartDTO cartDTO = new CartDTO();
        cartDTO.setUserId(cart.getUserId().getId());
        cartDTO.setCartDetailDTOList(cartDetailDTOs);
        return cartDTO;
    }
//
//    private List<CartDetailDTO> convertCartDetailsToDTO(List<CartDetail> cartDetails) {
//        return cartDetails.stream()
//                .map(cartDetail -> modelMapper.map(cartDetail, CartDetailDTO.class))
//                .collect(Collectors.toList());
//    }

    private CartDetailDTO convertToCartDetailDTO(CartDetail cartDetail) {
        CartDetailDTO cartDetailDTO = new CartDetailDTO();
        cartDetailDTO.setId(cartDetail.getId());
        cartDetailDTO.setProductName(cartDetail.getProductId().getName());
        cartDetailDTO.setQuantity(cartDetail.getQuantity());
        cartDetailDTO.setPrice(cartDetail.getPrice());
        cartDetailDTO.setUnitPrice(cartDetail.getUnitPrice());
        String imageLink = getFirstImageLink(cartDetail.getProductId());
        cartDetailDTO.setProductImageLink(imageLink);
        cartDetailDTO.setProductColor(cartDetail.getProductColor());

        return cartDetailDTO;
    }



    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }


}
