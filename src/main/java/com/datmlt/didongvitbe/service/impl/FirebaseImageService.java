package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.service.*;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.StorageClient;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Service
public class FirebaseImageService implements IImageService {

    @Autowired
    @Qualifier("firebaseImageServiceProperties")
    private Properties properties;
    @EventListener
    public void init(ApplicationReadyEvent event) {
        try {
            ClassPathResource serviceAccount = new ClassPathResource("firebase.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount.getInputStream()))
                    .setStorageBucket(properties.getBucketName())
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String getImageUrl(String name) {
        return String.format(properties.getImageUrl(), name);
    }

    @Override
    public String save(MultipartFile file) throws IOException {
        Bucket bucket = StorageClient.getInstance().bucket();
        String name = generateFileName(file.getOriginalFilename());
        Blob blob = bucket.create(name, file.getInputStream(), file.getContentType());

        // Get the public URL of the saved file
        String imageUrl = blob.signUrl(365, TimeUnit.DAYS).toString();

        return imageUrl;
    }


    @Override
    public String save(BufferedImage bufferedImage, String originalFileName) throws IOException {
        byte[] bytes = getByteArrays(bufferedImage, getExtension(originalFileName));
        Bucket bucket = StorageClient.getInstance().bucket();
        String name = generateFileName(originalFileName);
        bucket.create(name, bytes);
        return name;
    }

    @Override
    public void delete(String name) throws IOException {
        if (StringUtils.isEmpty(name)) {
            throw new IOException("Invalid file name");
        }
        String bucketName = "didongviet-5f99c.appspot.com";
        Storage storage = StorageOptions.newBuilder().setProjectId(bucketName).build().getService();
        BlobId blobId = BlobId.of(bucketName, name);
        boolean deleted = storage.delete(blobId);
        if (!deleted) {
            throw new IOException("File not found");
        }
    }


    @Data
    @Configuration
    @ConfigurationProperties(prefix = "firebase.json")
    public static class Properties {
        private String bucketName;
        private String imageUrl;
    }
}
