package com.datmlt.didongvitbe.service.impl;

import com.datmlt.didongvitbe.entity.CategoryP;
import com.datmlt.didongvitbe.repositories.CategoryPRepository;
import com.datmlt.didongvitbe.service.CategoryPService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryPServiceImpl  implements CategoryPService {
    private CategoryPRepository categoryPRepository;
    public CategoryPServiceImpl(CategoryPRepository categoryPRepository) {
        this.categoryPRepository = categoryPRepository;
    }
    @Override
    public CategoryP findCategoryPByName(String categoryPName) {
        CategoryP categoryP = categoryPRepository.findByName(categoryPName);
        return categoryP;
    }
    @Override
    @Transactional
    public CategoryP createCategory(CategoryP categoryP){
        return categoryPRepository.save(categoryP);
    }

    @Override
    @Transactional
    public CategoryP updateCategoryP(CategoryP categoryP){
        return categoryPRepository.save(categoryP);
    }

    @Override
    @Transactional
    public Boolean deleteCategoryP(Long id){
        if(categoryPRepository.existsById(id)){
            categoryPRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
