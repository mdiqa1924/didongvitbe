package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.entity.Account;
import com.datmlt.didongvitbe.repositories.AccountRepository;
import com.datmlt.didongvitbe.utils.exception.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomAccountDetailsService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Account Not Found with email: " + email));
        return CustomAccountDetails.build(account);
    }

    @Transactional
    public UserDetails loadAccountById(Long id) {
        Account account = accountRepository.findById(id).orElseThrow(
                () -> new ApiException("Account not found ", HttpStatus.NOT_FOUND)
        );

        return CustomAccountDetails.build(account);
    }
}
