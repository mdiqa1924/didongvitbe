package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.entity.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class CustomAccountDetails implements OAuth2User, UserDetails {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String fullname;

    private String email;

    private String password;

    private Collection<? extends GrantedAuthority> roles;

    private Map<String, Object> attributes;

    public CustomAccountDetails(Long id, String email, String password,
                                Collection<? extends GrantedAuthority> roles) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public static CustomAccountDetails build(Account account) {
        List<GrantedAuthority> roles = account.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        return new CustomAccountDetails(
                account.getId(),
                account.getEmail(),
                account.getPassword(),
                roles);
    }

    public static CustomAccountDetails create(Account account, Map<String, Object> attributes) {
        CustomAccountDetails customUserDetails = CustomAccountDetails.build(account);
        customUserDetails.setAttributes(attributes);
        return customUserDetails;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return fullname;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        CustomAccountDetails user = (CustomAccountDetails) o;
        return Objects.equals(id, user.id);
    }
    @Override
    public String getName() {
        return fullname;
    }
}
