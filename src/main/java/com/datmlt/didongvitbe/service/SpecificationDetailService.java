package com.datmlt.didongvitbe.service;

import com.datmlt.didongvitbe.dto.forCreate.CSpecificationDetailDTO;
import com.datmlt.didongvitbe.dto.forUpdate.USpecificationDetailDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface SpecificationDetailService {


    @Transactional
    List<CSpecificationDetailDTO> saveSpecificationDetails(Long productId, List<CSpecificationDetailDTO> cSpecificationDetailDTOList);

    @Transactional
    boolean updateSpecificationDetailByProductId(Long productId, List<USpecificationDetailDTO> specificationDetailDTO);

    List<Map<String, Object>>getListDetailById (Long productId);
}
