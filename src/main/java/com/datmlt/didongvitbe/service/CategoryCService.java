package com.datmlt.didongvitbe.service;

import com.datmlt.didongvitbe.entity.CategoryC;
import com.datmlt.didongvitbe.dto.forList.LProductDTO;
import java.util.List;
import java.util.Optional;

public interface CategoryCService {

    CategoryC createCategoryC(CategoryC category);

    List<CategoryC> getAllCategoriesC();

    Optional<CategoryC> getCategoryCById(Long categoryId);

    CategoryC updateCategoryC(CategoryC categoryC);

    boolean deleteCategoryCById(Long categoryCId);

    List<LProductDTO> getProductsByCategoryC(CategoryC categoryC);
}

