package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.entity.Payment;

import java.util.List;
import java.util.Optional;

public interface PaymentService {
    Payment createPayment(Payment payment);
    List<Payment> getAllPayment();
    Optional<Payment> getPaymentById(Long id);
    Payment updatePayment(Payment payment);
    boolean deletePaymentByTd(Long id);
}
