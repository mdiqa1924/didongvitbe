package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.dto.forCreate.CRequestVoucherDetail;
import com.datmlt.didongvitbe.entity.VoucherDetail;

public interface VoucherDetailService {
    VoucherDetail insert(CRequestVoucherDetail cRequestVoucherDetail);
}
