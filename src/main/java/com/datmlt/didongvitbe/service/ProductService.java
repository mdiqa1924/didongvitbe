package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.dto.forCreate.ProductDTO;
import com.datmlt.didongvitbe.dto.forCreate.ProductResponseDTO;
import com.datmlt.didongvitbe.dto.forDetail.DProductDTO;
import com.datmlt.didongvitbe.dto.forList.LProductDTO;
import com.datmlt.didongvitbe.dto.forUpdate.UProductDTO;
import com.datmlt.didongvitbe.entity.Product;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ProductService {
    @Transactional
    ProductResponseDTO addProduct(ProductDTO productDto);


    List<Product> getAllProducts();

    List<LProductDTO> searchProducts(String keyword);

    DProductDTO getProductById(Long productId);

    @Transactional
    UProductDTO updateProductInfo(UProductDTO updatedProductDto);


    @Transactional
    boolean deleteProduct(Long productId);

    Page<Product> getAllProducts(int page);

    Page<LProductDTO> getAllProductDTOs(int page, Sort.Direction direction);


}
