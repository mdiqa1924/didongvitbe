package com.datmlt.didongvitbe.service;



import com.datmlt.didongvitbe.dto.AccountDTO;
import com.datmlt.didongvitbe.dto.RegisterModel;
import com.datmlt.didongvitbe.entity.Account;
import com.datmlt.didongvitbe.security.jwt.JwtAuthResponse;

import java.util.List;
import java.util.Optional;

public interface AccountService {
    Optional<Account> getByEmail(String email);
    JwtAuthResponse validateLogin(String email, String password);
    Account create(RegisterModel registerModel);
    void delete(Long id);
    void changeRole(Long id);
    List<Account> getAllAccount();
    List<Account> getByRole(String role);
    void changeStatus(Long id);
    Account changePassword(AccountDTO.ChangePassword changePassword);
    AccountDTO.EmailResult sendResetPassword(AccountDTO.ChangePassword changePassword);
    Account resetPassword(AccountDTO.ChangePassword changePassword);

}
