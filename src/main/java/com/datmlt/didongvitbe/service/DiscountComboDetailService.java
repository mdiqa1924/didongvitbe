package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.dto.forCreate.CRequestDiscountComboDetail;
import com.datmlt.didongvitbe.entity.DiscountComboDetail;

import java.util.List;

public interface DiscountComboDetailService {
    DiscountComboDetail insert(CRequestDiscountComboDetail cRequestDiscountComboDetail);
    List<DiscountComboDetail> getlistByC1(Long idC1);

    //List<DiscountComboDetail> findBy2Cate(Long cate1, Long cate2);
    List<DiscountComboDetail> findBy2Cate(Long cate1, Long cate2);

    List<DiscountComboDetail> getAll();
}

