package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.dto.forList.LProductManufacturer;
import com.datmlt.didongvitbe.entity.Manufacturer;

import java.util.List;

public interface ManufacturerService {
    Manufacturer createManufacturer(Manufacturer manufacturer);

    Manufacturer updateManufacturer(Manufacturer manufacturer);

    Boolean deleteManufacturer(Long id);

    List<Manufacturer> getAllManufacturer();

    List<LProductManufacturer> getProductsByManufacturer(Manufacturer manufacturer);
}
