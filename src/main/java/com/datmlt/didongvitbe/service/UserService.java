package com.datmlt.didongvitbe.service;



import com.datmlt.didongvitbe.dto.RegisterModel;
import com.datmlt.didongvitbe.dto.UserDTO;
import com.datmlt.didongvitbe.entity.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {
    User create(RegisterModel registerModel);
    User update(MultipartFile multipartFile, Long id, UserDTO.Update updateUser);
    UserDTO.Response getResponseUserByUserId(Long id);
    UserDTO.Response getResponseUserByAccountId(Long id);
    User getUserByUserId(Long id);
    User getUserByAccountId(Long id);
    List<User> getAll();
}
