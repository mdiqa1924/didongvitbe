package com.datmlt.didongvitbe.service;



import com.datmlt.didongvitbe.entity.Store;

import java.util.List;
import java.util.Optional;

public interface StoreService {

    Store createStore(Store store);

    List<Store> getAllStores();

    Optional<Store> getStoreById(Long storeId);

    Store updateStore(Store store);

    boolean deleteStoreById(Long storeId);

    List<Store> findByName(String query);

    List<Store> findBy2Contains(String query1,String query2);

}
