package com.datmlt.didongvitbe.service;



import com.datmlt.didongvitbe.dto.forCreate.DiscountPaymentDTO;
import com.datmlt.didongvitbe.dto.forResponse.RDiscountPaymentDTO;
import com.datmlt.didongvitbe.entity.DiscountPayment;

import java.util.List;
import java.util.Optional;

public interface DiscountPaymentService {
    RDiscountPaymentDTO createDiscountPayment(DiscountPaymentDTO discountPayment);
    List<DiscountPayment> getAllDiscountPayment();
    Optional<DiscountPayment> getDiscountPaymentById(Long id);
    RDiscountPaymentDTO updateDiscountPayment(DiscountPaymentDTO discountPaymentDTO);
    boolean deleteDiscountPaymentByTd(Long id);
    List<DiscountPayment> findByDiscountPaymentNameContaining(String name);
}
