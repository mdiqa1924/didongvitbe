package com.datmlt.didongvitbe.service;



import com.datmlt.didongvitbe.dto.forDetail.CartDetailDTO;
import com.datmlt.didongvitbe.dto.forList.CartDTO;

import java.util.List;

public interface CartService {
    CartDTO getCartById(Long userId);

    public List<CartDetailDTO> getCartDetailsByCartId(Long userId);

    CartDTO addProductToCart(Long userId, Long productId, Long colorId);

    void clearCart(Long userId);
    void removeCartDetail(Long cartDetailId);
    CartDetailDTO updateCartDetail(Long cartDetailId, CartDetailDTO cartDetailDTO, int quantityChange);
    List<CartDetailDTO> getAllCartDetails(Long userId);
    public CartDTO getCartByUserId(Long userId);
}
