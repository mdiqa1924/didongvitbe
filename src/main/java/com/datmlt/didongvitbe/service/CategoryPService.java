package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.entity.CategoryP;

public interface CategoryPService {
    CategoryP createCategory(CategoryP categoryP);

    CategoryP updateCategoryP(CategoryP categoryP);

    Boolean deleteCategoryP(Long id);


    CategoryP findCategoryPByName(String name);
}
