package com.datmlt.didongvitbe.service;


import com.datmlt.didongvitbe.dto.forCreate.RateDTO;
import com.datmlt.didongvitbe.dto.forCreate.RateResponseDTO;
import com.datmlt.didongvitbe.dto.forList.LRateDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface RateService {
    RateResponseDTO createRate(RateDTO rateDTO, MultipartFile[] files);
    List<LRateDTO> getRateByProductId(Long productId);
}
