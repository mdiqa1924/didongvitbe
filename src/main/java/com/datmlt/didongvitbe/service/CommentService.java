package com.datmlt.didongvitbe.service;

import com.datmlt.didongvitbe.dto.forCreate.CommentDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {
    List<CommentDTO> getCommentsByProductId(Long productId);
    CommentDTO createComment(CommentDTO commentDTO);
    void deleteComment(Long id);
}
