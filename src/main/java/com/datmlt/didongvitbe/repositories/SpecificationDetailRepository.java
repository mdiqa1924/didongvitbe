package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.Product;
import com.datmlt.didongvitbe.entity.SpecificationDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SpecificationDetailRepository extends JpaRepository<SpecificationDetail, Long> {
    List<SpecificationDetail> findByProductId(Long productId);
    boolean existsByProductAndNameDetail(Product product, String nameDetail);

    @Modifying
    @Transactional
    @Query("DELETE FROM SpecificationDetail ds WHERE ds.product.id = :productId")
    void deleteByProductId(@Param("productId") Long productId);

}
