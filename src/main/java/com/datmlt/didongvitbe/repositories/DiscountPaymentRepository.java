package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.DiscountPayment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiscountPaymentRepository extends JpaRepository<DiscountPayment, Long> {
    List<DiscountPayment> findByDiscountPaymentNameContaining(String name);
}
