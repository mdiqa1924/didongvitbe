package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
}
