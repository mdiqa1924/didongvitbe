package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.VoucherDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoucherDetailRepository extends JpaRepository<VoucherDetail, Long> {
}
