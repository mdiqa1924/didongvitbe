package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.CategoryC;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryC, Long> {

}
