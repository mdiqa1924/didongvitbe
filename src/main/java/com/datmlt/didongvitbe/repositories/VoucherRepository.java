package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Long> {
    List<Voucher> findAllByVoucherNameContains(String s);
    Voucher findByVoucherCode(String code);
//    List<Voucher> findAllByVoucherNameContainsAndVoucherNameContaining(String a, String b);

}
