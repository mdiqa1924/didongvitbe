package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Cart;
import com.datmlt.didongvitbe.entity.CartDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartDetailRepository extends JpaRepository<CartDetail, Long> {
    List<CartDetail> findByCartId(Cart cartId);

}
