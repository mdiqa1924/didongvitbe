package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.NewsType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsTypeRepository extends JpaRepository<NewsType, Long> {
}
