package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.DiscountPaymentDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiscountPaymentDetailRepository extends JpaRepository<DiscountPaymentDetail, Long> {
    /*boolean existsByOrderId(Long orderId);
    Optional<DiscountPaymentDetail> findByOrderId(Long orderId);*/
}
