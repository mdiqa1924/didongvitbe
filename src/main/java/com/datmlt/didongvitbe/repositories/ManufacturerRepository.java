package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {

}

