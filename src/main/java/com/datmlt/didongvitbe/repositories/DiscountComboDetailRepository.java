package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.CategoryC;
import com.datmlt.didongvitbe.entity.DiscountComboDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiscountComboDetailRepository extends JpaRepository<DiscountComboDetail, Long> {
    List<DiscountComboDetail> findAllByCategoryC1_IdOrCategoryC2_Id(Long categoryCId1, Long categoryCId2);
    //List<DiscountComboDetail> findAllByCategoryC1_IdAndCategoryC2_Id(Long categoryCId1, Long categoryCId2);

    List<DiscountComboDetail> findAllByCategoryC1AndCategoryC2(CategoryC categoryCId1, CategoryC categoryCId2);
}

