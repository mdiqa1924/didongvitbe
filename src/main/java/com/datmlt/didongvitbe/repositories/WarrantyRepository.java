package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.Warranty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarrantyRepository extends JpaRepository<Warranty, Long> {
}
