package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.Role;
import com.datmlt.didongvitbe.utils.constant.AuthoritiesConstants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(AuthoritiesConstants name);
}

