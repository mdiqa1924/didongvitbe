package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RateRepository extends JpaRepository<Rate, Long> {
    List<Rate> findAllByProductId_Id(Long productId);

    @Transactional
    void deleteByProductId_Id(Long productId);
}
