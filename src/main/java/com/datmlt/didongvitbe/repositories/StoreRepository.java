package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StoreRepository extends JpaRepository<Store, Long> {
    List<Store> findAllByStoreNameContainsAndStoreNameContaining(String query1, String query2);
    List<Store> findAllByStoreNameContains(String query);
}
