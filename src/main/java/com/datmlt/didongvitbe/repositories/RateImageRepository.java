package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.RateImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RateImageRepository extends JpaRepository<RateImage, Long> {
    List<RateImage> findAllByRateId_Id(Long rateId);

    List<RateImage> findAllByRateId_ProductId_Id(Long productId);


    @Transactional
    void deleteByRateId_Id(Long id);
}
