package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.StoreDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreDetailRepository extends JpaRepository<StoreDetail, Long> {
}
