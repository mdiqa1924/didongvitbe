package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String email);
    Boolean existsByEmail(String email);
    @Query(value = "select * from Account a where a.email like %:keyword%", nativeQuery = true)
    List<Account> search(@Param("keyword") String keyword);
//    List<Account> findAllByEmailContains(String keyword);


}
