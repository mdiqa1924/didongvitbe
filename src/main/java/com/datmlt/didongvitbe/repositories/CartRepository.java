package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface CartRepository extends JpaRepository<Cart, Long> {
    Optional<Cart> findById(@Param("id")Long cartId);
    Cart findByUserId_Id(Long userId);
}
