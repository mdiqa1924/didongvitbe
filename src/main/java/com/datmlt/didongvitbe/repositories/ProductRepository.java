package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.CategoryC;
import com.datmlt.didongvitbe.entity.Manufacturer;
import com.datmlt.didongvitbe.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository

public interface ProductRepository extends JpaRepository<Product,Long> {
    List<Product> findByCategoryC(CategoryC categoryC);

    List<Product> findByManufacturer(Manufacturer manufacturer);
    List<Product> findByNameContainingIgnoreCase(String keyword);
    Page<Product> findAll(Pageable pageable);



}
