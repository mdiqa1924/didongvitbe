package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    @Query("SELECT c FROM Comment c WHERE c.productId.id = :productId")
    List<Comment> findAllByProductId(Long productId);

    @Transactional
    void deleteByProductId_Id(Long productId);
}
