package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsRepository extends JpaRepository<News, Long> {
}
