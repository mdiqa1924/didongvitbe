package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecificationRepository extends JpaRepository<Specification, Long> {
}
