package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.CategoryP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface CategoryPRepository extends JpaRepository<CategoryP, Long> {
     @Query("SELECT c FROM CategoryP c WHERE c.categoryPName = :name")
     CategoryP findByName(@Param("name") String name);
}