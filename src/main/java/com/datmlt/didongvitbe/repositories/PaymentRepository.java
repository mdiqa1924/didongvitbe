package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
