package com.datmlt.didongvitbe.repositories;

import com.datmlt.didongvitbe.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Orders, Long> {
}
