package com.datmlt.didongvitbe.repositories;


import com.datmlt.didongvitbe.entity.Color;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ColorRepository extends JpaRepository<Color, Long> {
}
