package com.datmlt.didongvitbe;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springdoc.core.configuration.SpringDocConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@OpenAPIDefinition
@Import({SpringDocConfiguration.class})
@SpringBootApplication
public class DidongvitbeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DidongvitbeApplication.class, args);
	}

}
