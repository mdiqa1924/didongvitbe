package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "specification")

public class Specification{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "specification_id")
    private long id;
    private String specificationName;
}
