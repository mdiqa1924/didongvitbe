package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;

    private String fullName;

    private String phoneNumber;

    private String email;

    private String address;

    private String status;

    @ManyToOne
    @JoinColumn(name = "paymentId")
    private Payment paymentId;

    @ManyToOne
    @JoinColumn(name = "storeId")
    private Store storeId;
}