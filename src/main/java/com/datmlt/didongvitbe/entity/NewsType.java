package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;
@Entity
@Data
@Table(name = "newsType")
public class NewsType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long newsTypeId;
    private String name;
    private String link;
}
