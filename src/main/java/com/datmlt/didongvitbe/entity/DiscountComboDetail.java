package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "discountComboDetail")
public class DiscountComboDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "discountComboDetail_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "categoryC_id1")
    private CategoryC categoryC1;

    @ManyToOne
    @JoinColumn(name = "categoryC_id2")
    private CategoryC categoryC2;

    @ManyToOne
    @JoinColumn(name = "discountCombo_id")
    private DiscountCombo discountCombo;
}

