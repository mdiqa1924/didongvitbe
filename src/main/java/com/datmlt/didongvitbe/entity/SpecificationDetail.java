package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "specificationDetail")
public class SpecificationDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "specificationDetail_id")
    private long id;
    private String nameDetail;
    private String description;
    @ManyToOne
    @JoinColumn(name = "specification_id")
    private Specification Specification;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

}
