package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "store")
public class Store {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "store_id")
    private Long id;

    private String storeName;

    @Column(length = 1000)
    private  String linkStoreMap;

    private  String phoneNumber;

}
