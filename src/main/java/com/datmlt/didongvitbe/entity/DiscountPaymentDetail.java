package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "discountPaymentDetail")
public class DiscountPaymentDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "discountPaymentDetail_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "discountPayment_id")
    private DiscountPayment discountPaymentId;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Orders orderId;

}