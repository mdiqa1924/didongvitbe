package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "storeDetail")
public class StoreDetail{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "storeDetail_id")
    private Long id;

    private Integer stock;

    @ManyToOne
    @JoinColumn(name = "storeId")
    private Store storeId;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product productId;
}