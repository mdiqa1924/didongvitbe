package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;



@Entity
@Data
@Table(name = "categoryc")
public class CategoryC {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "categoryc_id")
    private Long id;

    private String categoryName;

    @ManyToOne
    @JoinColumn(name = "Categoryp_id")
    private CategoryP categoryP;
}
