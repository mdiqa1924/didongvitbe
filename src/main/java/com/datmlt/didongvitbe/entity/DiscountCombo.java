package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "discountCombo")
public class DiscountCombo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "discountCombo_id")
    private Long id;

    private String discountName;

    private Double percent;

    private String dateStart;

    private String dateEnd;
}


