package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "discountPayment")
public class DiscountPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "discountPayment_id")
    private Long id;

    private String discountPaymentName;

    private Double percent;

    private String status;

    @ManyToOne
    @JoinColumn(name = "payment_id")
    private Payment paymentId;
}
