package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "categoryP")
public class CategoryP {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "categoryP_id")
    private Long id;

    private String categoryPName;
}
