package com.datmlt.didongvitbe.entity;
import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "voucherDetail")
public class VoucherDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "voucherDetail_id")
    private long id;

    private Long ordersID;

    @ManyToOne
    @JoinColumn(name = "voucher_id")
    private Voucher voucher;

    public VoucherDetail(Long ordersID, Voucher voucher) {
        this.ordersID = ordersID;
        this.voucher = voucher;
    }
    public VoucherDetail() {

    }
}




