package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "rateImage")
public class RateImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rateImage_id")
    private Long id;

    @Column(name = "link", length = 1000)
    private String link;

    @ManyToOne
    @JoinColumn(name = "rate_id")
    private Rate rateId;
}