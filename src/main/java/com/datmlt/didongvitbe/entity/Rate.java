package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "rate")
public class Rate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rate_id")
    private Long id;

    private String fullName;

    private String phoneNumber;

    private String evaluate;

    private Double star;

    private String Time;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product productId;
}
