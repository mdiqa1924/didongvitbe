package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product")
public class Product{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "productId")
    private Long id;

    private String name;
    @Column( length = 1000)
    private String description;

    private Long  price;

    private Integer stock;

    private String conditions;

    private Long discount;

    private String dateWarranty;

    private Double rate;

    @ManyToOne
    @JoinColumn(name = "manufacturer_id")
    private Manufacturer manufacturer;

    @ManyToOne
    @JoinColumn(name = "categoryc_id")
    private CategoryC categoryC;

}
