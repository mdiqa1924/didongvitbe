package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "news")
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long newsId;
    private String title;
    private String bannerImg;
    private String content;
    @Column(length = 20000)
    private String description;
    private String createAt;
    private String updateAt;
    private String createBy;
    private String linkContent;
    @ManyToOne
    private NewsType newsType;
}
