package com.datmlt.didongvitbe.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commentId")
    private Long id;

    private String phoneNumber;

    private String fullName;

    private String content;

    private String time;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product productId;
}