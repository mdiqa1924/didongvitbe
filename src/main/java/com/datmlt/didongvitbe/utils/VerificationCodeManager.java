package com.datmlt.didongvitbe.utils;

import java.util.HashMap;
import java.util.Map;

public class VerificationCodeManager {
    private static final Map<String, String> verificationCodes = new HashMap<>();
    public static void saveVerificationCode(String email, String verificationCode){
        verificationCodes.put(email, verificationCode);
    }
    public static String getVerificationCode(String email){
        return verificationCodes.get(email);
    }
    public static void removeVerificationCode(String email){
        verificationCodes.remove(email);
    }
    public static boolean isVerificationCodeValid(String email, String verificationCode){
        String savedVerificationCode = verificationCodes.get(email);
        return savedVerificationCode != null && savedVerificationCode.equals(verificationCode);
    }
}
