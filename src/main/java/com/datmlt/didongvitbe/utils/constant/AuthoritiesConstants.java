package com.datmlt.didongvitbe.utils.constant;

public enum AuthoritiesConstants {
    ROLE_CUSTOMER,
    ROLE_MANAGER,
    ROLE_ADMIN
}
