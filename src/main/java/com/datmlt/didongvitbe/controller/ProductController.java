package com.datmlt.didongvitbe.controller;


import com.datmlt.didongvitbe.dto.forCreate.*;
import com.datmlt.didongvitbe.dto.forDetail.*;
import com.datmlt.didongvitbe.dto.forList.LProductDTO;
import com.datmlt.didongvitbe.dto.forUpdate.UProductDTO;
import com.datmlt.didongvitbe.entity.Product;
import com.datmlt.didongvitbe.service.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Tag(name = "Product", description = "Product API's")
@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductColorService productColorService;
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @PostMapping("/add")
    public ResponseEntity<ProductResponseDTO> addProduct(@ModelAttribute("productDTO") ProductDTO productDto) {
        ProductResponseDTO responseDTO = productService.addProduct(productDto);
        Long productId = responseDTO.getProduct().getId();
        List<Long> colors = productDto.getColors(); // Get the list of colors from the ProductDTO
        Map<String, Object> createdProductColor = productColorService.createProductColor(productId, colors);
        List<Map<String, Object>> productColors = (List<Map<String, Object>>) createdProductColor.get("productColors");
        responseDTO.setProductColors(productColors);
        return ResponseEntity.ok().body(responseDTO);
    }


    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {
        List<Product> products = productService.getAllProducts();
        return ResponseEntity.ok(products);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<DProductDTO> getProductById(@PathVariable Long productId) {
        DProductDTO productDTO = productService.getProductById(productId);

        if (productDTO == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(productDTO);
    }

    @DeleteMapping("delete/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable Long productId) {
        boolean deleted = productService.deleteProduct(productId);
        if (deleted) {
            return ResponseEntity.ok("Product deleted successfully");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/update")
    public ResponseEntity<UProductDTO> updateProduct(@ModelAttribute UProductDTO updatedProductDto) {
        UProductDTO updatedProduct = productService.updateProductInfo(updatedProductDto);
        return ResponseEntity.ok(updatedProduct);
    }

    @GetMapping("/search")
    public ResponseEntity<List<LProductDTO>> searchProductsByName(@RequestParam("keyword") String keyword) {
        List<LProductDTO> products = productService.searchProducts(keyword);
        return ResponseEntity.ok(products);
    }
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public ResponseEntity<Page<LProductDTO>> getAllProductDTOs(@RequestParam(defaultValue = "0" ) int page, @RequestParam(defaultValue = "ASC") Sort.Direction direction) {
        Page<LProductDTO> productDTOs = productService.getAllProductDTOs(page, direction);
        return ResponseEntity.ok(productDTOs);
    }

    @RequestMapping(value = "/admin/page", method = RequestMethod.GET)
    public ResponseEntity<Page<Product>> getAllProducts(@RequestParam(defaultValue = "0") int page) {
        Page<Product> products = productService.getAllProducts(page);
        return ResponseEntity.ok(products);
    }


}
