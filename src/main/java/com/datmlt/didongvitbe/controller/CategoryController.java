package com.datmlt.didongvitbe.controller;


import com.datmlt.didongvitbe.dto.forList.LProductDTO;
import com.datmlt.didongvitbe.entity.CategoryC;
import com.datmlt.didongvitbe.service.CategoryCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryCService categoryCService;
    @PostMapping
    public ResponseEntity<CategoryC> createCategory(@RequestBody CategoryC category) {
        CategoryC createdCategory = categoryCService.createCategoryC(category);
        return new ResponseEntity<>(createdCategory, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<CategoryC>> getAllCategories() {
        List<CategoryC> categories = categoryCService.getAllCategoriesC();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CategoryC> updateCategory(@PathVariable("id") Long categoryId, @RequestBody CategoryC category) {
        Optional<CategoryC> existingCategory = categoryCService.getCategoryCById(categoryId);
        if (existingCategory.isPresent()) {
            category.setId(categoryId);
            CategoryC updatedCategory = categoryCService.updateCategoryC(category);
            return new ResponseEntity<>(updatedCategory, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable("id") Long categoryId) {
        boolean deleted = categoryCService.deleteCategoryCById(categoryId);
        if (deleted) {
            return new ResponseEntity<>("Category deleted successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Category not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<List<LProductDTO>> getProductsByCategoryC(@PathVariable Long categoryId) {
        CategoryC categoryC = new CategoryC();
        categoryC.setId(categoryId);
        List<LProductDTO> products = categoryCService.getProductsByCategoryC(categoryC);
        return ResponseEntity.ok(products);
    }
}
