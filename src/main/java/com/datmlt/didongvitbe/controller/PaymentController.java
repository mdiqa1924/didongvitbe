package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.entity.Payment;
import com.datmlt.didongvitbe.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @PostMapping("/add")
    public ResponseEntity<Payment> createPayment(@RequestBody Payment payment){
        Payment createdPayment = paymentService.createPayment(payment);
        return new ResponseEntity<>(createdPayment, HttpStatus.CREATED);
    }
    @GetMapping("/all")
    public ResponseEntity<List<Payment>> getAllPayment(){
        List<Payment> payments = paymentService.getAllPayment();
        return new ResponseEntity<>(payments, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Payment> getPaymentById(@PathVariable("id") Long id){
        Optional<Payment> payment = paymentService.getPaymentById(id);
        if (payment.isPresent()) {
            return new ResponseEntity<>(payment.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Payment> getPaymentById(@PathVariable("id") Long id, @RequestBody Payment payment){
        Optional<Payment> exitPayment = paymentService.getPaymentById(id);
        if (exitPayment.isPresent()) {
            payment.setId(id);
            Payment updatePayment = paymentService.updatePayment(payment);
            return new ResponseEntity<>(updatePayment, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deletePayment(@PathVariable("id") Long id) {
        boolean deleted = paymentService.deletePaymentByTd(id);
        if (deleted) {
            return new ResponseEntity<>("Payment deleted successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Payment not found", HttpStatus.NOT_FOUND);
    }

}
