package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.forCreate.DiscountPaymentDetailDTO;
import com.datmlt.didongvitbe.entity.DiscountPaymentDetail;
import com.datmlt.didongvitbe.service.DiscountPaymentDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/discountpaymentdetail")
public class DiscountPaymentDetailController {
    @Autowired
    private DiscountPaymentDetailService discountPaymentDetailService;
    @PostMapping("/add")
    public ResponseEntity<DiscountPaymentDetail> createDiscountPaymentDetail(@RequestBody DiscountPaymentDetailDTO discountPaymentDetailDTO){
        DiscountPaymentDetail createdDiscountPaymentDetail = discountPaymentDetailService.createDiscountPaymentDetail(discountPaymentDetailDTO);
        return new ResponseEntity<>(createdDiscountPaymentDetail, HttpStatus.CREATED);
    }
    @GetMapping("/all")
    public ResponseEntity<List<DiscountPaymentDetail>> getAllDiscountPaymentDetail(){
        List<DiscountPaymentDetail> discountPaymentDetails = discountPaymentDetailService.getAllDiscountPaymentDetail();
        return new ResponseEntity<>(discountPaymentDetails, HttpStatus.OK);
    }
    /*@GetMapping("/check")
    public ResponseEntity<RCheckDiscount> getDiscountPaymentsByName(@RequestBody CheckDiscount id) {
        RCheckDiscount rCheckDiscount = discountPaymentDetailService.existByOrderId(id);
        return new ResponseEntity<>(rCheckDiscount, HttpStatus.OK);
    }*/
}
