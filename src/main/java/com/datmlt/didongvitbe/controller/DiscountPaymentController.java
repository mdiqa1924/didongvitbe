package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.forCreate.DiscountPaymentDTO;
import com.datmlt.didongvitbe.dto.forResponse.RDiscountPaymentDTO;
import com.datmlt.didongvitbe.entity.DiscountPayment;
import com.datmlt.didongvitbe.service.DiscountPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/discountpayment")
public class DiscountPaymentController {
    @Autowired
    private DiscountPaymentService discountPaymentService;
    @PostMapping("/add")
    public ResponseEntity<RDiscountPaymentDTO> createDiscountPayment(@RequestBody DiscountPaymentDTO discountPayment){
        RDiscountPaymentDTO createdDiscountPayment = discountPaymentService.createDiscountPayment(discountPayment);
        return new ResponseEntity<>(createdDiscountPayment, HttpStatus.CREATED);
    }
    @GetMapping("/all")
    public ResponseEntity<List<DiscountPayment>> getAllDiscountPayment(){
        List<DiscountPayment> discountPayments = discountPaymentService.getAllDiscountPayment();
        return new ResponseEntity<>(discountPayments, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<DiscountPayment> getDiscountPaymentById(@PathVariable("id") Long id){
        Optional<DiscountPayment> discountPayments = discountPaymentService.getDiscountPaymentById(id);
        if (discountPayments.isPresent()) {
            return new ResponseEntity<>(discountPayments.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<RDiscountPaymentDTO> updateDiscountPayment(@PathVariable("id") Long id, @RequestBody DiscountPaymentDTO discountPaymentDTO) {
        Optional<DiscountPayment> existingDiscountPayment = discountPaymentService.getDiscountPaymentById(id);
        if (existingDiscountPayment.isPresent()) {
            discountPaymentDTO.setId(id);
            RDiscountPaymentDTO updatedDiscountPayment = discountPaymentService.updateDiscountPayment(discountPaymentDTO);
            return new ResponseEntity<>(updatedDiscountPayment, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteDiscountPayment(@PathVariable("id") Long id) {
        boolean deleted = discountPaymentService.deleteDiscountPaymentByTd(id);
        if (deleted) {
            return new ResponseEntity<>("Discount payment deleted successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Discount payment not found", HttpStatus.NOT_FOUND);
    }
    @GetMapping("/search/{name}")
    public ResponseEntity<List<DiscountPayment>> getDiscountPaymentsByName(@PathVariable("name") String name) {
        List<DiscountPayment> discountPayments = discountPaymentService.findByDiscountPaymentNameContaining(name);
        if (!discountPayments.isEmpty()) {
            return new ResponseEntity<>(discountPayments, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
