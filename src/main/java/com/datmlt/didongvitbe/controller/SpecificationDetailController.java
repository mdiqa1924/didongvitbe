package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.forCreate.CSpecificationDetailDTO;
import com.datmlt.didongvitbe.dto.forUpdate.USpecificationDetailDTO;
import com.datmlt.didongvitbe.service.SpecificationDetailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/details")
public class SpecificationDetailController {
    private final SpecificationDetailService specificationDetailService;

    public SpecificationDetailController(SpecificationDetailService specificationDetailService) {
        this.specificationDetailService = specificationDetailService;
    }

    @PostMapping("/{productId}")
    public ResponseEntity<List<CSpecificationDetailDTO>> saveSpecificationDetails(@PathVariable Long productId,
                                                                                  @RequestBody List<CSpecificationDetailDTO> cSpecificationDetailDTOList)
    {
        List<CSpecificationDetailDTO> savedSpecificationDetails = specificationDetailService.saveSpecificationDetails(productId, cSpecificationDetailDTOList);
        return ResponseEntity.ok(savedSpecificationDetails);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<List<Map<String, Object>>> getListDetailByProductId(@PathVariable Long productId) {
        List<Map<String, Object>> specificationDetails = specificationDetailService.getListDetailById(productId);
        return ResponseEntity.ok(specificationDetails);
    }

    @PutMapping("/{productId}")
    public ResponseEntity<String> updateSpecificationDetailsByProductId(@PathVariable Long productId, @RequestBody List<USpecificationDetailDTO> specificationDetailDTO) {
        boolean updateSuccessful = specificationDetailService.updateSpecificationDetailByProductId(productId, specificationDetailDTO);

        if (updateSuccessful) {
            return ResponseEntity.ok("Specification details updated successfully");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update specification details");
        }
    }



}