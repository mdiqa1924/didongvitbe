package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.entity.Store;
import com.datmlt.didongvitbe.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/stores")
public class StoreController {
    @Autowired
    private StoreService storeService;
    @PostMapping("/add")
    public ResponseEntity<Store> createStore(@RequestBody Store store) {
        Store createdStore = storeService.createStore(store);
        return new ResponseEntity<>(createdStore, HttpStatus.CREATED);
    }
    @GetMapping("/get-list")
    public ResponseEntity<List<Store>> getAllStore() {
        List<Store> stores = storeService.getAllStores();
        return new ResponseEntity<>(stores, HttpStatus.OK);
    }
    @GetMapping("/edit/{id}")
    public ResponseEntity<Store> getStoreById(@PathVariable("id") Long storeId) {
        Optional<Store> store = storeService.getStoreById(storeId);
        if (store.isPresent()) {
            return new ResponseEntity<>(store.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PutMapping("/update")
    public ResponseEntity<Store> updateStore(@RequestBody Store store) {
        Store updatedColor = storeService.updateStore(store);
        return new ResponseEntity<>(updatedColor, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteStore(@PathVariable("id") Long storeId) {
        boolean deleted = storeService.deleteStoreById(storeId);
        if (deleted) {
            return new ResponseEntity<>("Store deleted successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Store not found", HttpStatus.NOT_FOUND);
    }
    @GetMapping("/search")
    public ResponseEntity<List<Store>> findStoreName(@RequestParam("query") String query) {
        List<Store> findStoreName = storeService.findByName(query);
        if(findStoreName.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(findStoreName, HttpStatus.OK);
    }
    @GetMapping("/search2")
    public ResponseEntity<List<Store>> findStoreBy2Name(@RequestParam("query1") String query1, @RequestParam("query2") String query2) {
        List<Store> findStoreWith2Query = storeService.findBy2Contains(query1, query2);
        if(findStoreWith2Query.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(findStoreWith2Query, HttpStatus.OK);
    }
}
