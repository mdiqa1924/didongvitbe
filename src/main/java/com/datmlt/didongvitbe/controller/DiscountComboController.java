package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.forCheck.CKListCart;
import com.datmlt.didongvitbe.dto.forCreate.CRequestDiscountCombo;
import com.datmlt.didongvitbe.dto.forCreate.CRequestDiscountComboDetail;
import com.datmlt.didongvitbe.dto.forList.RLListCart;
import com.datmlt.didongvitbe.entity.*;
import com.datmlt.didongvitbe.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/discount-combo")
public class DiscountComboController {
    @Autowired
    DiscountComboDetailService discountComboDetailService;
    @Autowired
    DiscountComboService discountComboService;
    @Autowired
    ProductService productService;

    //add a discount combo
    @PostMapping("/add")
    public ResponseEntity<DiscountCombo> addDiscountCombo(@RequestBody CRequestDiscountCombo cRequestDiscountCombo) {
        return new ResponseEntity<DiscountCombo>(discountComboService.insert(cRequestDiscountCombo), HttpStatus.CREATED);
    }
    // get discount combo by id
    @GetMapping("/get")
    public ResponseEntity<DiscountCombo> getDiscountCombo(@RequestParam("id") Long id){
        return new ResponseEntity<DiscountCombo>(discountComboService.getOne(id),HttpStatus.OK);
    }
    //get list discount combo
    @GetMapping("/list")
    public ResponseEntity<List<DiscountCombo>> getAllDiscountCombo(){
        List<DiscountCombo> discountComboList = discountComboService.getAll();
        return new ResponseEntity<List<DiscountCombo>>(discountComboList, HttpStatus.OK);
    }
    //update a discount combo
    @PostMapping("/update")
    public ResponseEntity<DiscountCombo> updateDiscountCombo(@RequestBody DiscountCombo discountCombo) {
        return new ResponseEntity<DiscountCombo>(discountComboService.update(discountCombo),HttpStatus.OK);
    }
    //find discount combo by name
    @GetMapping("/get-by-name")
    public ResponseEntity<List<DiscountCombo>> getDiscountComboByName(@RequestParam("keyword") String string){
        List<DiscountCombo> discountComboList = discountComboService.search(string);
        return new ResponseEntity<List<DiscountCombo>>(discountComboList, HttpStatus.OK);
    }

    @GetMapping("/check")
    public ResponseEntity<List<RLListCart>> checkDiscountCombo(@RequestBody List<CKListCart> listCarts){
        List<RLListCart> rlListCarts = checkList(listCarts);
        return new ResponseEntity<List<RLListCart>>(rlListCarts, HttpStatus.OK);
    }
    public List<RLListCart> checkList(List<CKListCart> listCarts){
        List<RLListCart> rlListCarts = new ArrayList<>();
        for (int i = 0; i < listCarts.size(); i++){
            Double max = 0.0;
            int index = 0;
            Long id1 = listCarts.get(i).getProductId();
            for(int j = 0; j < listCarts.size(); j++){
                Long id2 = listCarts.get(j).getProductId();
                List<DiscountComboDetail> discountComboDetail = discountComboDetailService.findBy2Cate(id1,id2);
                if(discountComboDetail != null){
                    for(DiscountComboDetail discountComboDetail1 : discountComboDetail){
                        LocalDate start = LocalDate.parse(discountComboDetail1.getDiscountCombo().getDateStart());
                        LocalDate end = LocalDate.parse(discountComboDetail1.getDiscountCombo().getDateEnd());
                        if(checkDate(start,end) == 1){
                            Double percent = discountComboDetail1.getDiscountCombo().getPercent();
                            if (max<percent){
                                max = percent;
                            }
                        }
                    }
                }
            }
            RLListCart rlListCart = new RLListCart();
            rlListCart.setProductId(id1);
            rlListCart.setTotal(productService.getProductById(id1).getProduct().getDiscount()*(100-max)/100);
            rlListCarts.add(rlListCart);
        }
        return rlListCarts;
    }
    public int checkDate(LocalDate localDateStart, LocalDate localDateEnd){
        LocalDate date = LocalDate.now();
        if(date.isEqual(localDateStart) || date.isEqual(localDateEnd) || (date.isAfter(localDateStart) && date.isBefore(localDateEnd))){
            return 1;
        }
        return 0;
    }
    // add discount combo detail - admin
    @PostMapping("/add-detail")
    public ResponseEntity<DiscountComboDetail> addDiscountComboDetail(@RequestBody CRequestDiscountComboDetail cRequestDiscountComboDetail) {
        return new ResponseEntity<DiscountComboDetail>(discountComboDetailService.insert(cRequestDiscountComboDetail),HttpStatus.CREATED);
    }
    //can sua thanh dto thi noi
    @GetMapping("/list-detail")
    public ResponseEntity<List<DiscountComboDetail>> getAll() {
        return new ResponseEntity<List<DiscountComboDetail>>(discountComboDetailService.getAll(),HttpStatus.CREATED);
    }

    @GetMapping("/get2")
    public ResponseEntity<List<DiscountComboDetail>> get2(@RequestParam("id1") Long id1, @RequestParam("id2") Long id2 ) {
        return new ResponseEntity<List<DiscountComboDetail>>(discountComboDetailService.findBy2Cate(id1,id2),HttpStatus.CREATED);
    }
}

