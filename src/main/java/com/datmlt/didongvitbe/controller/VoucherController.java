package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.forCheck.CkVoucher;
import com.datmlt.didongvitbe.dto.forCreate.CRequestVoucher;
import com.datmlt.didongvitbe.dto.forCreate.CRequestVoucherDetail;
import com.datmlt.didongvitbe.dto.forDetail.RResponseVoucherDetail;
import com.datmlt.didongvitbe.dto.forList.RLResponseVoucher;
import com.datmlt.didongvitbe.entity.Voucher;
import com.datmlt.didongvitbe.entity.VoucherDetail;
import com.datmlt.didongvitbe.service.VoucherDetailService;
import com.datmlt.didongvitbe.service.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/voucher")
public class VoucherController {
    @Autowired
    VoucherService voucherService;
    @Autowired
    VoucherDetailService voucherDetailService;
    //add a voucher
    @PostMapping("/add")
    public ResponseEntity<Voucher> addVoucher(@RequestBody CRequestVoucher cRequestVoucher) {
        if(voucherService.check(cRequestVoucher.getVoucherCode()) != null){
            Voucher voucher2 = new Voucher();
            return new ResponseEntity<Voucher>(voucher2, HttpStatus.valueOf("Code already exist"));
        }
        else{
            System.out.println(cRequestVoucher.toString());
            return new ResponseEntity<Voucher>(voucherService.insert(cRequestVoucher), HttpStatus.CREATED);
        }

    }
    //Get a voucher by id
    @GetMapping("/get")
    public ResponseEntity<Voucher> getVoucher(@RequestParam("id") Long id){
        Voucher voucher = voucherService.getOne(id);
        System.out.println(voucher.toString());
        if(voucher != null){
            return new ResponseEntity<Voucher>(voucher, HttpStatus.OK);
        }
        else {
            Voucher voucher1 = new Voucher();
            return new ResponseEntity<Voucher>(voucher1, HttpStatus.OK);
        }

    }
    //Get all vouchers
    @GetMapping("/list")
    public ResponseEntity<List<RLResponseVoucher>> getAllVoucher(){
        List<RLResponseVoucher> RLResponseVoucherList = voucherService.getALl();
        return new ResponseEntity<List<RLResponseVoucher>>(RLResponseVoucherList, HttpStatus.OK);
    }
    //update a voucher
    @PostMapping("/update")
    public ResponseEntity<Voucher> updateVoucher(@RequestBody Voucher voucher){
        Voucher voucher1 = voucherService.check(voucher.getVoucherCode());

        if(voucher1 == null || voucher1.getId() == voucher.getId()){
            return new ResponseEntity<Voucher>(voucherService.update(voucher), HttpStatus.CREATED);
        }
        else{
            Voucher voucher2 = new Voucher();
            return new ResponseEntity<Voucher>(voucher2, HttpStatus.valueOf("Code already exist"));
        }
    }
    //search voucher when name contains string
    @GetMapping("/getAllByName")
    public ResponseEntity<List<RLResponseVoucher>> getAllVoucherByName(@RequestParam("keyword") String string){
        List<RLResponseVoucher> RLResponseVoucherList = voucherService.search(string);
        return new ResponseEntity<List<RLResponseVoucher>>(RLResponseVoucherList, HttpStatus.OK);
    }
    //check voucher is available
    @GetMapping("/check")
    public ResponseEntity<RResponseVoucherDetail> checkVoucherDetail(@RequestBody CkVoucher ckVoucher) {
        Voucher voucher = voucherService.check(ckVoucher.getVoucherCode());
        RResponseVoucherDetail rResponseVoucherDetail = new RResponseVoucherDetail();
        rResponseVoucherDetail.setTotal(ckVoucher.getTotal());
        if(voucher != null){
            LocalDate start = LocalDate.parse(voucher.getDateStart());
            LocalDate end = LocalDate.parse(voucher.getDateEnd());
            if(checkDate(start,end) == 1) {
                if(ckVoucher.getTotal() >= voucher.getConditions() && voucher.getStock() > 0){
                    //VoucherDetail voucherDetail = voucherDetailService.insert(cRequestVoucherDetail);
                    rResponseVoucherDetail.setTotal(ckVoucher.getTotal() - voucher.getDiscountValue());
                    rResponseVoucherDetail.setMsg("Success");
                    return new ResponseEntity<RResponseVoucherDetail>(rResponseVoucherDetail, HttpStatus.valueOf(200));
                }
                else if(ckVoucher.getTotal() < voucher.getConditions()){
                    rResponseVoucherDetail.setMsg("The total not enough or out of vouchers");
                    return new ResponseEntity<RResponseVoucherDetail>(rResponseVoucherDetail, HttpStatus.valueOf(412));
                }
            }
            CRequestVoucherDetail cRequestVoucherDetail1 = new CRequestVoucherDetail();
            rResponseVoucherDetail.setMsg("The voucher has expired or it has not started");
            return new ResponseEntity<RResponseVoucherDetail>(rResponseVoucherDetail, HttpStatus.valueOf(412));
        }
        else {
            CRequestVoucherDetail cRequestVoucherDetail1 = new CRequestVoucherDetail();
            rResponseVoucherDetail.setMsg("The voucher not exist");
            return new ResponseEntity<RResponseVoucherDetail>(rResponseVoucherDetail, HttpStatus.valueOf(412));
        }
    }


    public int checkDate(LocalDate localDateStart, LocalDate localDateEnd){
        LocalDate date = LocalDate.now();
        if(date.isEqual(localDateStart) || date.isEqual(localDateEnd) || (date.isAfter(localDateStart) && date.isBefore(localDateEnd))){
            return 1;
        }
        return 0;
    }

    //add voucher detail when check successfully
    @PostMapping("/addVoucherDetail")
    public ResponseEntity<VoucherDetail> addVoucherDetail(@RequestBody CRequestVoucherDetail cRequestVoucherDetail) {
        Voucher voucher = voucherService.check(cRequestVoucherDetail.getVoucherCode());
        voucher.setStock(voucher.getStock()-1);
        Voucher voucher2 = voucherService.update(voucher);
        VoucherDetail voucherDetail = voucherDetailService.insert(cRequestVoucherDetail);

        return new ResponseEntity<VoucherDetail>(voucherDetail, HttpStatus.CREATED);

    }
}
