package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.ProductColorDTO;
import com.datmlt.didongvitbe.dto.UProductColorDTO;
import com.datmlt.didongvitbe.service.ProductColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/product_color")
public class ProductColorController {
    @Autowired
    private ProductColorService productColorService;
//    @PostMapping("/add/{productId}")
//    public ResponseEntity<Map<String, Object>> createProductColor(@PathVariable Long productId, @RequestBody List<CProductColorDTO> cProductColorDTOList) {
//
//        Map<String, Object> createdProductColor = productColorService.createProductColor(productId, cProductColorDTOList);
//        return ResponseEntity.ok(createdProductColor);
//    }

    @GetMapping("/get-list/{productId}")
    public ResponseEntity<ProductColorDTO> getListDetailById(@PathVariable Long productId) {
        ProductColorDTO productColor = productColorService.getListDetailById(productId);
        return ResponseEntity.ok(productColor);
    }
    @PutMapping("/edit/{productId}")
    public ResponseEntity<ProductColorDTO> updateProductColor(@PathVariable Long productId, @RequestBody UProductColorDTO uProductColorDTO) {
        ProductColorDTO productColorDTO = productColorService.updateProductColors(productId, uProductColorDTO.getColors());
        return ResponseEntity.ok(productColorDTO);
    }



}