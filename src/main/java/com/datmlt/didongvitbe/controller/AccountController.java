package com.datmlt.didongvitbe.controller;


import com.datmlt.didongvitbe.dto.*;
import com.datmlt.didongvitbe.entity.*;
import com.datmlt.didongvitbe.repositories.*;
import com.datmlt.didongvitbe.security.jwt.JwtAuthResponse;
import com.datmlt.didongvitbe.security.jwt.JwtUtil;
import com.datmlt.didongvitbe.service.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@Tag(name = "Account", description = "Account API's")
@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private RoleRepository authorityRepository;

    @Value("${ADMIN_EMAIL}")
    private String ADMIN_EMAIL;

    @Value("${ADMIN_PASSWORD}")
    private String ADMIN_PASSWORD;

    @PostMapping(value = "/register")
    public ResponseEntity<Object> registerAccount(@Valid @RequestBody RegisterModel registerModel){
        Account newAccount = accountService.create(registerModel);
        User newUser = userService.create(registerModel);
        return ResponseEntity.ok().body(newUser);
    }

    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity<Object> loginAccount(@Valid @RequestParam("email") String email, @RequestParam("password") String password,
                                               HttpServletResponse response) throws AuthenticationException {
        try{
            JwtAuthResponse jwtAuthResponse = accountService.validateLogin(email, password);
            String jwt = jwtAuthResponse.getToken();
            AccountDTO.Response accountResponse = jwtAuthResponse.getAccount();
            return ResponseEntity.ok(new JwtAuthResponse(jwt, accountResponse));
        }catch (AuthenticationException ae){
            return new ResponseEntity<>(Collections.singletonMap("AuthenticationException", ae.getLocalizedMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

//    @GetMapping(value = "/{email}")
//    public ResponseEntity<Account> getAccountByEmail(@PathVariable("email") String email){
//        Account account = accountService.getByEmail(email);
//        return ResponseEntity.ok().body(account);
//    }

    @GetMapping(value = "/")
    @PreAuthorize("hasRole('CUSTOMER') or hasRole('ADMIN')")
    public ResponseEntity<List<Account>> getAccounts(){
        List<Account> accounts = accountService.getAllAccount();
        return ResponseEntity.ok().body(accounts);
    }

    @GetMapping(value = "/changeRole", produces = "application/json")
    public ResponseEntity<String> changeAccountRole(@Valid @RequestParam("id") Long id){
        accountService.changeRole(id);
        return ResponseEntity.ok().body("{\"message\": \"Update role successfully!\"}");
    }

    @GetMapping(value = "/changeStatus", produces = "application/json")
    public ResponseEntity<String> changeAccountStatus(@Valid @RequestParam("id") Long id){
        accountService.changeStatus(id);
        return ResponseEntity.ok().body("{\"message\": \"Update status successfully!\"}");
    }

    @GetMapping(value = "/role")
    public ResponseEntity<List<Account>> getAccountByRole(@RequestParam("role") String role){
        List<Account> accountsByRole = accountService.getByRole(role);
        return ResponseEntity.ok(accountsByRole);
    }

    @GetMapping(value = "/search")
    public ResponseEntity<List<Account>> search(@RequestParam("keyword") String keyword){
        List<Account> searchList = accountRepository.search(keyword);
        return ResponseEntity.ok(searchList);
    }

    @PostMapping(value = "/changePassword", produces = "application/json")
    public ResponseEntity<String> changePassword(@Valid @RequestBody AccountDTO.ChangePassword changePassword){
        accountService.changePassword(changePassword);
        return ResponseEntity.ok().body("{\"message\": \"Change password successfully!\"}");
    }

    @PostMapping("/reset")
    public ResponseEntity<AccountDTO.EmailResult> sendResetPassword(@RequestBody AccountDTO.ChangePassword changePassword) {
        try {
            AccountDTO.EmailResult result = accountService.sendResetPassword(changePassword);
            return ResponseEntity.ok(result);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new AccountDTO.EmailResult(false, e.getMessage()));
        }
    }

    @PostMapping("/reset/confirm")
    public ResponseEntity<Account> resetPassword(@Valid @RequestBody AccountDTO.ChangePassword changePassword) {
        try {
            Account account = accountService.resetPassword(changePassword);
            return ResponseEntity.ok(account);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

//    @PostConstruct
//    public void createAdmin() {
//        String email = "admin@gmail.com";
//        if (!accountRepository.existsByEmail(email)) {
//
//            Role role = new Role();
//            role.setName(AuthoritiesConstants.ROLE_ADMIN);
//            authorityRepository.save(role);
//
//
//            String password = passwordEncoder.encode("password");
//            Optional<Role> authority = authorityRepository.findByName(AuthoritiesConstants.ROLE_ADMIN);
//            Set<Role> authorities = new HashSet<>();
//            Account newAdmin = new Account();
//            newAdmin.setEmail(email);
//            newAdmin.setStatus("ACTIVE");
//            newAdmin.setProvider(AuthProvider.local);
//            newAdmin.setPassword(password);
//            authority.ifPresent(auth -> authorities.add(auth));
//            newAdmin.setRoles(authorities);
//            Account resultAccount = accountRepository.save(newAdmin);
//
//
//            User user = new User();
//            user.setFullName("ADMIN");
//            user.setPhoneNumber("0523467814");
//            user.setDateOfBirth("12/7/2023");
//            user.setAvatar("https://firebasestorage.googleapis.com/v0/b/didongviet-5f99c.appspot.com/o/z4506645735041_37c7c30d5a45340fbf62c96e5988caa6.jpg?alt=media&token=14c30eda-0c99-444b-ab87-c9997abe3775");
//            user.setCreateAt(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
//            user.setAccount(resultAccount);
//            User resultUser = userRepository.save(user);
//
//            Cart newCart = new Cart();
//            newCart.setUserId(resultUser);
//            cartRepository.save(newCart);
//        }
//    }
}
