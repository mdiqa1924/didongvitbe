package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.forCreate.*;
import com.datmlt.didongvitbe.dto.forList.LRateDTO;
import com.datmlt.didongvitbe.service.RateService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(value = "/api/rates")
public class RateController {
    private final RateService rateService;

    public RateController(RateService rateService){
        this.rateService = rateService;
    }
    @GetMapping("/product/{productId}")
    public ResponseEntity<List<LRateDTO>> getRateByProductId(@PathVariable Long productId){
        List<LRateDTO> rates = rateService.getRateByProductId(productId);
        return new ResponseEntity<>(rates, HttpStatus.OK);
    }

    @PostMapping("/createRate")
    public ResponseEntity<RateResponseDTO> createRate(@ModelAttribute RateDTO rateDTO, @RequestParam("files") MultipartFile[] files) {
        RateResponseDTO responseDTO = rateService.createRate(rateDTO, files);
        return ResponseEntity.ok().body(responseDTO);
    }

}
