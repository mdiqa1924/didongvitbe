package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.*;
import com.datmlt.didongvitbe.entity.*;
import com.datmlt.didongvitbe.repositories.*;
import com.datmlt.didongvitbe.service.*;
import com.datmlt.didongvitbe.service.impl.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Tag(name = "User", description = "User API's")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private FirebaseImageService firebaseImageService;

    @PostMapping(value = "/update")
    public ResponseEntity<Object> update(@Valid @RequestParam("id") Long id, @RequestParam(value = "avatar", required = false) MultipartFile multipartFile, UserDTO.Update updateUser) throws IOException{
        User result = userService.update(multipartFile, id, updateUser);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getUser(@RequestParam("id") Long id ){
        UserDTO.Response user = userService.getResponseUserByUserId(id);
        return ResponseEntity.ok().body(user);
    }

    @GetMapping(value = "/users")
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> users = userService.getAll();
        return ResponseEntity.ok().body(users);
    }

    @GetMapping(value = "/account")
    public ResponseEntity<User> getUserByAccountId(@RequestParam("accountId") Long id){
        User user = userService.getUserByAccountId(id);
        return ResponseEntity.ok().body(user);
    }
}
