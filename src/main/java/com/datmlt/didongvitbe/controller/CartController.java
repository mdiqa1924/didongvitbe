package com.datmlt.didongvitbe.controller;

import com.datmlt.didongvitbe.dto.forDetail.CartDetailDTO;
import com.datmlt.didongvitbe.dto.forList.CartDTO;
import com.datmlt.didongvitbe.service.CartService;
import com.google.api.gax.rpc.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/carts")

public class CartController {
    @Autowired
    private CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<CartDTO> getCartById(@PathVariable Long userId) {
        CartDTO cartDTO = cartService.getCartById(userId);
        return ResponseEntity.ok(cartDTO);
    }

    @PostMapping("/users")
    public ResponseEntity<CartDTO> addProductToCart(
            @RequestParam Long userId,
            @RequestParam Long productId,
            @RequestParam Long colorId
    ) {
        CartDTO cartDTO = cartService.addProductToCart(userId, productId, colorId);
        return ResponseEntity.ok(cartDTO);
    }

    @PostMapping("/clear/{cartId}")
    public ResponseEntity<String> clearCart(@PathVariable Long cartId) {
        try {
            cartService.clearCart(cartId);
            return ResponseEntity.ok("Cart cleared successfully.");
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/remove-cart-detail/{cartDetailId}")
    public ResponseEntity<Void> removeCartDetail(@PathVariable Long cartDetailId) {
        cartService.removeCartDetail(cartDetailId);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{cartDetailId}")
    public ResponseEntity<CartDetailDTO> updateCartDetail(
            @PathVariable Long cartDetailId,
            @RequestBody CartDetailDTO cartDetailDTO,
            @RequestParam("quantityChange") int quantityChange
    ) {
        try {
            CartDetailDTO updatedCartDetailDTO = cartService.updateCartDetail(cartDetailId, cartDetailDTO, quantityChange);
            return ResponseEntity.ok(updatedCartDetailDTO);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("/all-cart-details")
    public ResponseEntity<List<CartDetailDTO>> getAllCartDetails(@RequestParam Long userId) {
        List<CartDetailDTO> cartDetails = cartService.getAllCartDetails(userId);
        return ResponseEntity.ok(cartDetails);
    }

    @GetMapping("/user")
    public ResponseEntity<CartDTO> getCartByUserId(@RequestParam Long userId) {
        CartDTO carts = cartService.getCartByUserId(userId);
        return ResponseEntity.ok(carts);
    }
}
