package com.datmlt.didongvitbe.security.oauth2.service;

import com.datmlt.didongvitbe.dto.UserDTO;
import com.datmlt.didongvitbe.entity.Account;
import com.datmlt.didongvitbe.entity.Cart;
import com.datmlt.didongvitbe.entity.Role;
import com.datmlt.didongvitbe.entity.User;
import com.datmlt.didongvitbe.repositories.AccountRepository;
import com.datmlt.didongvitbe.repositories.CartRepository;
import com.datmlt.didongvitbe.repositories.RoleRepository;
import com.datmlt.didongvitbe.repositories.UserRepository;
import com.datmlt.didongvitbe.security.oauth2.account.OAuth2AccountInfo;
import com.datmlt.didongvitbe.security.oauth2.account.OAuth2AccountInfoFactory;
import com.datmlt.didongvitbe.service.CustomAccountDetails;
import com.datmlt.didongvitbe.service.UserService;
import com.datmlt.didongvitbe.utils.constant.AuthProvider;
import com.datmlt.didongvitbe.utils.constant.AuthoritiesConstants;
import com.datmlt.didongvitbe.utils.exception.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CustomOAuth2AccountService extends DefaultOAuth2UserService {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private RoleRepository authorityRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

        try {
            return processOAuth2Account(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2Account(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        OAuth2AccountInfo oAuth2AccountInfo = OAuth2AccountInfoFactory.getOAuth2AccountInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());
        if(StringUtils.isEmpty(oAuth2AccountInfo.getEmail())) {
            throw new ApiException("Email not found from OAuth2 provider", HttpStatus.NOT_FOUND);
        }

        Optional<Account> accountOptional = accountRepository.findByEmail(oAuth2AccountInfo.getEmail());
        Account account;
        if(accountOptional.isPresent()) {
            account = accountOptional.get();
            if(!account.getProvider().equals(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()))) {
                throw new ApiException("Looks like you're signed up with " +
                        account.getProvider() + " account. Please use your " + account.getProvider() +
                        " account to login.", HttpStatus.NOT_FOUND);
            }
            account = updateExistingAccount(account, oAuth2AccountInfo);
        } else {
            account = registerNewAccount(oAuth2UserRequest, oAuth2AccountInfo);
        }

        return CustomAccountDetails.create(account, oAuth2User.getAttributes());
    }

    private Account registerNewAccount(OAuth2UserRequest oAuth2UserRequest, OAuth2AccountInfo oAuth2AccountInfo) {
        Optional<Role> authority = authorityRepository.findByName(AuthoritiesConstants.ROLE_CUSTOMER);
        Set<Role> authorities = new HashSet<>();
        Account account = new Account();
        account.setProvider(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()));
        account.setProviderId(oAuth2AccountInfo.getId());
        account.setEmail(oAuth2AccountInfo.getEmail());
        authority.ifPresent(auth -> authorities.add(auth));
        account.setRoles(authorities);
        account.setStatus("ACTIVE");
        registerNewUser(oAuth2AccountInfo);
        return accountRepository.save(account);
    }

    private User registerNewUser(OAuth2AccountInfo oAuth2AccountInfo){
        User newUser = new User();
        newUser.setFullName(oAuth2AccountInfo.getName());
        newUser.setAvatar(oAuth2AccountInfo.getImageUrl());
        newUser.setCreateAt(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        User result = userRepository.save(newUser);

        Cart newCart = new Cart();
        newCart.setUserId(result);
        cartRepository.save(newCart);
        return result;
    }

    private Account updateExistingAccount(Account existingAccount, OAuth2AccountInfo oAuth2AccountInfo) {
        existingAccount.setEmail(oAuth2AccountInfo.getEmail());
        UserDTO.Response existingUserResponse = userService.getResponseUserByAccountId(existingAccount.getId());
        User existingUser = userService.getUserByUserId(existingUserResponse.getId());
        existingUser.setAvatar(oAuth2AccountInfo.getImageUrl());
        userRepository.save(existingUser);
        return accountRepository.save(existingAccount);
    }
}
